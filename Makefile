# A simple makefile to build the docs.
#
# Usage:
#
# To build the docs locally (needs hugo, ImageMagick and python correctly instaled) and serve them on port 8080:
# $ make pages  # build locally
#
# To build the docs in a docker container (needs docker installed) and serve them on port 8080:
# $ make docker # build via docker
#
# Both commands take optional arguments SRC_OCT and NAME.
# SRC_OCT is the path to the octopus source (if not passed octopus folder is assumed at current working dir).
# NAME is the display name for the web pages (default: custom)
# Example:
# $ make SRC_OCT=full_path_to_octopus_repo NAME="main" pages   # build locally with name as main
# $ make SRC_OCT=full_path_to_octopus_repo NAME="test" docker   # build via docker with name as test


SRC_OCT ?= $(pwd)/octopus
NAME ?= custom
pages:
	# Check for dependencies
	$(shell if ! which hugo > /dev/null; then echo "hugo could not be found. Please install hugo."; exit 1; fi)
	$(shell if ! which convert > /dev/null; then echo "ImageMagick could not be found. Please install ImageMagick."; exit 2; fi)
	$(shell if ! which python3 > /dev/null; then echo "python3 could not be found. Please install python3."; exit 3; fi)	
	# check that themes/hugo-theme-docdock exists and is not empty, if not, clone it
	if [ ! -d "themes/hugo-theme-docdock" ] || [ ! -d "themes/hugo-theme-docdock/.git" ]; then \
		git submodule update --init --recursive; \
	fi
	# Build the docs and serve them on port 8080
	bash build-branch.sh -d $(SRC_OCT) -n $(NAME)
	xdg-open "http://localhost:8080/$(NAME)/"
	python3 -m http.server 8080 --directory public

image:
	if ! which docker > /dev/null; then echo "docker could not be found. Please install docker."; exit 4; fi
	docker build -f Dockerfile -t octopus-docs .

docker:image
	echo "Mounting  octopus directory $(SRC_OCT) and running on port 8080."
	echo "Open http://localhost:8080/$(NAME) to view the docs"
	docker run -it --rm -v $(SRC_OCT):/home/user/octopus -p 8080:8080 octopus-docs /home/user/docs/docker/docker.sh -n $(NAME)

docker-debug:image
	echo "Mounting  octopus directory $(SRC_OCT) and running on port 8080."
	echo " Run docker/docker.sh to build the docs."
	docker run -it --rm -v $(SRC_OCT):/home/user/octopus -p 8080:8080 octopus-docs /bin/bash
clean:
	rm -rf content
	rm -rf public

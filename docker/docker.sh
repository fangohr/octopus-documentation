#!/bin/bash
if [ ! -d /home/user/octopus ]; then
    echo "Octopus source repo not mounted. Exiting."
    exit 1
fi
if [ -d /home/user/docs/octopus ]; then
    rm -rf /home/user/docs/octopus
fi
# Clone the theme if it doesn't exist
pushd /home/user/docs/themes/hugo-theme-docdock
git clone https://github.com/vjeantet/hugo-theme-docdock.git .
popd

# Build the docs
# copy the source repo to the docs folder
cp -r /home/user/octopus /home/user/docs

# build the docs
bash /home/user/docs/build-branch.sh "$@"

# Serve the docs

# via apache
# cp -r /home/user/docs/public/* /usr/local/apache2/htdocs/
# httpd-foreground

# via python
python -m http.server 8080 --directory /home/user/docs/public
---
title: "FAQ"
description: "Frequently asked questions"
weight: 100
---


This is a collection of frequently asked questions about {{<octopus>}}. 
<!--For other queries, please subscribe to <tt>octopus-users</tt> mailing list, and ask.-->

## Compilation 
### Could not find library... 

This is probably the most common error you can get. {{<octopus>}} uses several different libraries, the most important of which are <tt>gsl</tt>, <tt>fftw</tt>, and <tt>blas</tt>/<tt>lapack</tt>. We assume that you have already installed these libraries but, for some reason, you were not able to compile the code. So, what went wrong?

* Did you pass the correct <tt>--with-XXXX</tt> (where <tt>XXXX</tt> is <tt>gsl</tt>, <tt>fftw</tt> or <tt>lapack</tt> in lowercase) to the configure script? If your libraries are installed in a non-standard directory (like <tt>/opt/lapack</tt>), you will have to pass the script the location of the library (in this example, you could try <tt>./configure --with-lapack='-L/opt/lapack -llapack'</tt>.

* If you are working on an alpha station, do not forget that the <tt>CXML</tt> library includes <tt>BLAS</tt> and <tt>LAPACK</tt>, so it can be used by {{<octopus>}}. If needed, just set the correct path with <tt>--with-lapack</tt>.

* If the configuration script cannot find <tt>FFTW</tt>, it is probable that you did not compile <tt>FFTW</tt> with the same Fortran compiler or with the same compiler options. The basic problem is that Fortran sometimes converts the function names to uppercase, at other times to lowercase, and it can add an "_" to them, or even two. Obviously all libraries and the program have to use the same convention, so the best is to compile everything with the same Fortran compiler/options. If you are a power user, you can check the convention used by your compiler using the command <tt>nm &lt;library&gt;</tt>.

* Unfortunately, the libraries compiled with one Fortran compiler are very often not compatible with file compiled with another Fortran compiler. In order to avoid problems, please make sure that <i>all</i> libraries are compiles using the same Fortran compiler.

To compile the parallel version of of the code, you will also need MPI (mpich or LAM work just fine).

###  Error while loading shared libraries  

Sometimes, when you run {{<octopus>}} you stumble in the following error:

{{< code-block >}}

octopus: error while loading shared libraries: libXXX.so: cannot open
shared object file: No such file or directory
{{< /code-block >}}
</pre>

This is a classical problem of the dynamical linker. {{<octopus>}} was compiled dynamically, but the dynamical libraries (the .so files) are in a place that is not recognized by the dynamical loader. So, the solution is to tell the dynamical linker where the library is.

The first thing you should do is to find where the library is situated in your own system. Try doing a <tt>locate libXXX.so</tt>, for example. Let us imagine that the library is in the directory <tt>/opt/intel/compiler70/ia32/lib/</tt> (this is where the ifc7 libraries are by default). Now, if you do not have root access to the machine, just type (using the bash shell)

{{< code-block >}}

> export LD_LIBRARY_PATH=/opt/intel/compiler70/ia32/lib/:$LD_LIBRARY_PATH
> octopus
{{< /code-block >}}
</pre>

or
{{< code-block >}}

> LD_LIBRARY_PATH=/opt/intel/fc/9.0/lib/:$LD_LIBRARY_PATH octopus
{{< /code-block >}}
</pre>

If you have root control over the machine, you can use a more permanent alternative. Just add a line containing <tt>/opt/intel/compiler70/ia32/lib/</tt> to the file <tt>/etc/ld.so.conf</tt>. This file tells the dynamic linker where to find the <tt>.so</tt> libraries. Then you have to update the cache of the linker by typing

{{< code-block >}}

> ldconfig
{{< /code-block >}}
</pre>

A third solution is to compile octopus statically. This is quite simple in some systems (just adding -static to {{< code LDFLAFS >}}, or something like that), but in some others not (with my home ifc7 setup it's a real mess!)

###  What is METIS?  

When running parallel in "domains", octopus divides the simulation region (the box) into separate regions (domains) and assigns each of these to a different processor. This lets you not only speed up the calculation, but also divide the memory among the different processors. The first step of this process, the splitting of the box, is in general a very complicated process. Note that we are talking about a simulation box of an almost arbitrary shape, and of an arbitrary number of processors. Furthermore, as the communication between processors grows proportionally to the surface of the domains, one should use an algorithm that divides the box such that each domain has the same number of points, and at the same time minimizes the total area of the domains. The [METIS](http://www-users.cs.umn.edu/~karypis/metis/) library does just that. If you want to run octopus parallel in domains, you are required to use it (or the parallel version, ParMETIS). Currently METIS is included in the Octopus distribution and is compiled by default when MPI is enabled.

##  Running  

###  Sometimes I get a segmentation fault when running {{< octopus >}}  

The most typical cause for segmentation faults are caused by a limited stack size. Some compilers, especially the Intel one, use the stack to create temporary arrays, and when running large calculations the default stack size might not be enough. The solution is to remove the limitation in the size of the stack by running the command

{{< code-block >}}
 ulimit -s unlimited
{{< /code-block >}}

Segmentation faults can also be caused by other problems, like a wrong compilation (linking with libraries compiled with a different fortran compiler, for example) or a bug in the code.

###  How do I run parallel in domains?  

First of all, you '''must''' have a version of {{<octopus>}} compiled with support to the [METIS](http://www-users.cs.umn.edu/~karypis/metis/) library. This is a very useful library that takes care of the division of the space into domains. Then you just have to run octopus in parallel (this step depends on your actual system, you may have to use mpirun or mpiexec to accomplish it). 

In some run modes (e.g., <tt>td</tt>), you can use multi-level parallelization, ''i.e.'', run in parallel in more than one way at the same time. In the <tt>td</tt> case, you can run parallel in states and in domains at the same time. In order to fine-tune this behavior, please take a look at the variables <tt>ParallelizationStrategy</tt> and <tt>ParallelizationGroupRanks</tt>. In order to check if everything is OK, take a look at the output of octopus in section  "Parallelization". This is an example:

{{< code-block >}}

************************** Parallelization ***************************
Octopus will run in *parallel*
Info: Number of nodes in par_states  group:     8 (      62)
Info: Octopus will waste at least  9.68% of computer time
**********************************************************************
{{< /code-block >}}
</pre>

In this case, {{<octopus>}} runs in parallel only in states, using 8 processors (for 62 states). Furthermore, {{<octopus>}} some of the processors will be idle for 9.68% of the time (this is not so great, so maybe a different number of processors would be better in this case).

###  How to center the molecules?  

See {{< manual "External utilities:oct-center-geom" "External utilities:oct-center-geom" >}}.

<!--
###  How do I visualize 3D stuff?  

Our preferred visualization tool is [openDX](http://www.opendx.org). This is perhaps the most powerful 3D visualization tool for scientific data, and is highly versatile and sophisticated. However, this does not come for free: [openDX](http://www.opendx.org) is notoriously difficult to learn and to use. Anyway, in our opinion, its advantages clearly compensate for this problem.

The good news is that we made most of the dirty work for you! We have developed a small dx application that takes care of most of the details. If you want to try, start by installing [openDX](http://www.opendx.org). This is simpler in some machines than in others. For example, in my Fedora Core 6, I simply have to type

{{< code-block >}}
  yum install dx dx-devel dx-samples
{{< /code-block >}}

Note that we also need the development package. Next we have to install the chemical extensions to [openDX](http://www.opendx.org). You can find some instructions [[Releases-OpenDX|here]].

Now generate some files for visualization. These can be either in <tt>.dx</tt> or <tt>.ncdf</tt> format (see {{< variable "Output" >}} and {{< variable "OutputHow" >}}). Next copy the files {{< code "[prefix]/octopus/share/util/mf.net" >}} and {{< code "[prefix]/octopus/share/util/mf.cfg" >}} to your working directory and type

{{< code-block >}}
 dx mf.net
{{< /code-block >}}

Then in the dx menus, choose {{< code "Windows>Open Control Panel by Name>Main" >}}. You will see a dialog box with some options. Play with it!

Also see {{< manual "Visualization" "Visualization" >}}.
-->

###  Out of memory?  

'''Q:''' From time to time, I obtain the following error when I perform some huge memory-demanding calculations:

{{< code-block >}}
 **************************** FATAL ERROR *****************************
 *** Fatal Error (description follows)
 *--------------------------------------------------------------------
 * Failed to allocate     185115 Kb in file 'gs.F90' line    62
 *--------------------------------------------------------------------
 * Stack:
 **********************************************************************
{{< /code-block >}}

Could it be related to the fact that the calculation demands more memory than available in the computer?

'''A:''' Octopus doesn't allocate memory in a big piece, but allocates small chunks
as needed. So when you ask for more memory than is available, the allocation can fail even when asking for an innocent amount of memory. So yes, if you see this it is likely that you are running out of memory.

If you compiled on a 32-bit machine, you will be limited to a little more than 2 GB.  

##  Varia  

###  How do the version numbers in {{< octopus >}} work?  

Each stable release is identified by two numbers (for example x.y). The first number indicates a particular version of {{< octopus >}} and the second one the release. Before the 6.0 release (2016) Octopus used a three-number scheme, with the first two numbers being the version and the third one the release.

An increase in the revision number indicates that this release contains bug fixes and minor changes over the previous version, but that no new features were added (it may happen that we disable some features that are found to contain serious bugs).

Development versions (that you can get from the git repository) do not have a version number, they only have a code name. For code names we use the scientific names of [Octopus species](http://en.wikipedia.org/wiki/Octopus_%28genus%29). These are the code names that we have used so far (this scheme was started after the release of version 3.2):

* ''Octopus bimaculatus'': current version
* ''Octopus maya'': 12.x
* ''Octopus arboresces'': 11.x
* ''Octopus selene'' (moon octopus): 10.x 
* ''Octopus australis'' (hammer octopus): 9.x
* ''[Octopus wolfi](http://en.wikipedia.org/wiki/Octopus_wolfi)'' (star-sucker pygmy octopus): 8.x
* ''[Octopus mimus](http://en.wikipedia.org/wiki/Octopus_mimus)'' (Gould octopus): 7.x
* ''[Octopus tetricus](http://en.wikipedia.org/wiki/Octopus_tetricus)'' (common Sydney octopus or gloomy octopus): 6.x
* ''Octopus superciliosus'' (frilled pygmy octopus): 5.0.x
* ''Octopus nocturnus'': 4.1.x
* ''[Octopus vulgaris](http://en.wikipedia.org/wiki/Common_octopus)'' (common octopus): 4.0.x

###  Units  

See {{< manual "Basics/Units" "Units" >}}

###  How do I cite octopus?  

See {{< versioned-link "Citing_Octopus" "Citing Octopus" >}}.

---
title: "Unfolding"
#tags: ["Tutorial", "Expert", "Ground State", "Bulk", "oct-unfold"]
difficulties: "expert"
system_types: "bulk"
calculation_modes: "Ground state"
utilities: "oct_unfold"
description: "How to perform a band-structure unfolding."
---



In this tutorial, we look at how to perform a band-structure unfolding using Octopus. 

This calculation is done in few steps, which are described below. First, we need to compute the ground state of a supercell.

##  Input  
Here we start with the input file for Octopus. This is a ground state calculation for bulk silicon, similar to the one of the tutorial {{< tutorial "Periodic_systems" "Periodic Systems" >}}.
In the present case however, we will use a supercell of Si, composed of 8 atoms. 

{{< code-block >}}
 {{< variable "CalculationMode" >}} = gs
 
 {{< variable "PeriodicDimensions" >}} = 3
 BoxShape = parallelepiped
 {{< variable "Spacing" >}} = 0.5
 
 a = 10.18
 %{{< variable "LatticeParameters" >}}
  a | a  | a
 90 | 90 |90
 %
 
 %{{< variable "ReducedCoordinates" >}}
  "Si" | 0.0         | 0.0       | 0.0 
  "Si" |   1/2       | 1/2       | 0.0
  "Si" |   1/2       | 0.0       | 1/2
  "Si" |   0.0       | 1/2       | 1/2
  "Si" |   1/4       | 1/4       | 1/4
  "Si" |   1/4 + 1/2 | 1/4 + 1/2 | 1/4
  "Si" |   1/4 + 1/2 | 1/4       | 1/4 + 1/2
  "Si" |   1/4       | 1/4 + 1/2 | 1/4 + 1/2
 %
 
 nk = 4
 %{{< variable "KPointsGrid" >}}
   nk |  nk |  nk
 %
 {{< variable "KPointsUseSymmetries" >}} = yes
 
 {{< variable "ConvRelDens" >}} = 1e-7
 {{< variable "EigensolverTolerance" >}} = 1e-8
{{< /code-block >}}


Most of these variables are already used in other tutorials, so the interested read could refer for instance to {{< tutorial "Periodic_systems" "Periodic Systems" >}} for a detailed description.

##  Unfolding - step 1   

After running Octopus using the previous input file, we have the obtained the ground state of the supercell.
We now what to define the primitive cell on which we want to unfold or supercell, and the specific k-point path that we are interested in.

This is done by adding to the input file the following lines:

{{< code-block >}}
  {{< variable "UnfoldMode" >}} = unfold_setup
  %{{< variable "UnfoldLatticeParameters" >}}
    a | a | a
  %
  %{{< variable "UnfoldLatticeVectors" >}}
   0.  | 0.5 | 0.5
   0.5 | 0.  | 0.5
   0.5 | 0.5 | 0.0  
  %
  %{{< variable "UnfoldKPointsPath" >}}
   4 | 4 | 8
   0.5 | 0.0 | 0.0 - L point
   0.0 | 0.0 | 0.0 - Gamma point
   0.0 | 0.5 | 0.5 - X point
   1.0 | 1.0 | 1.0 - Another Gamma point
  %
{{< /code-block >}}

Lets see more in detail some of the input variables:

* <tt>{{< variable "UnfoldMode" >}} = unfold_setup</tt>: this variable instruct the utility <tt>oct-unfold</tt> in which mode we are running. As a first step, we are runing in the <tt>unfold_setup</tt> mode, which generates necessary files for getting the unfolded band structure.

* <tt>{{< variable "UnfoldLatticeParameters" >}}</tt> specifies the lattice parameters of the primitive cell. This variable is similar to {{< variable "LatticeParameters" >}}.

* <tt>{{< variable "UnfoldLatticeVectors" >}}</tt> specifies the lattice vectors of the primitive cell. This variable is similar to {{< variable "LatticeVectors" >}}.

* <tt>{{< variable "UnfoldKPointsPath" >}}</tt> specifies the k-point path for the unfolded. The coordinates are indicated as reduced coordinates for the primitive lattice. This variable is similar to {{< variable "KPointsPath" >}}.

By running <tt>oct-unfold</tt>, you will obtain two files. The first one (<tt>unfold_kpt.dat</tt>) contains the list of k-points of the specified k-point path, but expressed in reduced coordinates of the supercell. These are the k-points for which we need to evaluate the wavefunctions of the supercell.
The second file (<tt>unfold_gvec.dat</tt>) contains the list of reciprocal lattice vectors that relate the k-points of the primitive cell to the one in the supercell.

The unfolding still been an experimental feature, you also need to add to your input file the line
{{< code-block >}}
  ExperimentalFeatures = yes
{{< /code-block >}}

##  Unfolding - step 2  

In order to perform the unfolding, we must compute wavefunctions in the supercell at specific k-points. These points are described in the file <tt>unfold_kpt.dat</tt>.
To get them, we need to run an non self-consistent calculation using
{{< code-block >}}
  {{< variable "CalculationMode" >}} = unocc
{{< /code-block >}}

In order to use the list of k-points obtained in step 1, we remove from the input file the k-point related variables (%{{< variable "KPointsGrid" >}}, %{{< variable "KPointsPath" >}}, or %{{< variable "KPoints" >}}) and we replace them by the line
{{< code-block >}}
  include unfold_kpt.dat
{{< /code-block >}}

We then run {{< octopus >}} on this input file. 
Note that in order to get extra states, one should use the variables {{< variable "ExtraStates" >}} and {{< variable "ExtraStatesToConverge" >}}, as for a normal band structure calculation, 
see {{< tutorial "Periodic_systems" "Periodic Systems" >}}.

For this tutorial, we used

{{< code-block >}}
  {{< variable "ExtraStates" >}} = 6
  {{< variable "ExtraStatesToConverge" >}} = 4
  MaximumIter = 100
{{< /code-block >}}

Note that after you performed this step, you cannot run a TD calculation in this folder.
Indeed, the original GS states have been replaced by the one of this unocc calculation.

Here, we are requesting to converge 4 unoccupied states, which all correspond to the first valence band in the folded primitive cell, as the supercell is 4 times larger than the initial cell.

##  Unfolding - step 3  

Now that we have computed the states for the required k-points, we can finally compute the spectral function for each of the k-points of the specified k-point path.
This is done by changing unfolding mode to be
{{< code-block >}}
  {{< variable "UnfoldMode" >}} = unfold_run
{{< /code-block >}}

This will produce the spectral function for the full path (<tt>static/ake.dat</tt>) and for each individual points of the path (<tt>static/ake_XXX.dat</tt>). 
The unfolded bandstructure of silicon is shown below.

{{< figure src="/images/Si_unfolded.png" width="500px" caption="Unfolded band structure of bulk silicon." >}}

Note that in order to control the energy range and the energy resolution for the unfolded band structure, one can specify the variables {{< variable "UnfoldMinEnergy" >}}, {{< variable "UnfoldMaxEnergy" >}}, and {{< variable "UnfoldEnergyStep" >}}.

It is important to note here that the code needs to read all the unoccupied wavefunctions, and therefore will need to have enough memory.








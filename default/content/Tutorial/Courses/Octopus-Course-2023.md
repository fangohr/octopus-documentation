---
Title: "Octopus course 2023"
weight: 21
---


### Octopus Training Courses in 2023:


#### Octopus Basics (Sep. 11-15)

Day 1 (11 Sep):
- {{< versioned-link "/presentations/Octopus_2023/octopus_2023.pdf" "Short presentation of the theory and code features" >}}
- Octopus basics tutorial series

Day 2 (12 Sep):
- {{< versioned-link "/presentations/Octopus_2023/theory_to_implementation.pdf" "From the theory to practical numerical implementations" >}}
- {{< versioned-link "/presentations/Octopus_2023/octopus_optical_absorption.pdf" "Optical absorption tutorial series" >}}

Day 3 (13 Sep):

- {{< versioned-link "presentations/Octopus_2023/tutorials_solids.pdf" "Solids tutorial series" >}}
- {{< versioned-link "presentations/Octopus_2023/tutorials_models.pdf" "Model systems" >}} 

Day 4 (14 Sep):
- {{< versioned-link "presentations/Octopus_2023/postopus-slides.pdf" "Postopus introduction" >}}
- [Postopus tutorial](https://notebooks.mpcdf.mpg.de/binder/v2/git/https%3A%2F%2Fgitlab.mpcdf.mpg.de%2Fpostopus%2Fpostopus.git/main?labpath=dev%2FTutorials%2FReadme.ipynb)

Days 5 (15 Sep):
- Maxwell tutorials
- Free project [students choose one or more tutorials that haven't been covered yet]


#### Octopus Advanced (Sep. 25-29)

Day 1 (25 Sep):
- {{< versioned-link "/presentations/advanced_octopus_2023/octopus_overview.pdf" "Overview over the philosophy of Octopus" >}}
- {{< versioned-link "/presentations/advanced_octopus_2023/coding_standards.pdf" "Coding paradigms and object orientation in Octopus" >}}


Day 2 (26 Sep):
- {{< versioned-link "/presentations/advanced_octopus_2023/advanced_git.pdf" "Some advanced topics in *git*" >}}
- {{< versioned-link "/presentations/advanced_octopus_2023/testing.pdf" "Testing and CI in Octopus" >}}
- {{< versioned-link "/presentations/advanced_octopus_2023/code_structure.pdf" "The structure of Octopus" >}}

Day 3 (27 Sep):
- {{< versioned-link "/presentations/advanced_octopus_2023/mesh_and_grid.pdf" "Mesh and grids" >}}
- {{< versioned-link "/presentations/advanced_octopus_2023/mesh-functions_and_batches.pdf" "Mesh functions and batches" >}}
- {{< versioned-link "presentations/advanced_octopus_2023/multisystem_framework.pdf" "The multisystem framework" >}}


Day 4 (28 Sep):
- {{< versioned-link "presentations/advanced_octopus_2023/multisystem_classes.pdf" "Classes of the multisystem framework" >}}
- {{< versioned-link "presentations/advanced_octopus_2023/multisystem_example.pdf" "Example: celestial dynamics" >}}


Days 5 (29 Sep):
- {{< versioned-link "presentations/advanced_octopus_2023/hpc.pdf" "High Performance Computing" >}}
- {{< versioned-link "presentations/advanced_octopus_2023/debugging_and_profiling.pdf" "Debugging and Profiling" >}}
- {{< versioned-link "presentations/advanced_octopus_2023/development_summary.pdf" "Summary" >}}

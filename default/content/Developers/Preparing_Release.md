---
title: "Preparing a Release"
section: "Developers"
---


##  Major releases  

For a new major release (_e.g._ 10.0):
* Create a release branch from {{<file "develop" >}}. The branch should be named {{<file "release-10.0" >}}.
* Update the version number in {{<code "AC_INIT" >}} in {{< file "configure.ac" >}} and push the changes to the git repository.
* The {{<buildbot>}} will automatically create from the release branch a tarball [http://octopus-code.org/down.php?file=release-10.0/octopus-10.0.tar.gz](http://octopus-code.org/down.php?file=release-10.0/octopus-10.0.tar.gz), the  {{<versioned-link "variables" "variable documentation" >}}, and the doxygen documentation [https://octopus-code.org/doc/release-10.0/doxygen_doc/index.html](https://octopus-code.org/doc/release-10.0/doxygen_doc/index.html).
* Distribute the tarball among the developers for testing and correct any problems encountered.
* Update the {{<file "PACKAGING" >}} file. (This requires to update the periodic_systems/09-etsf_io.test)
* Update the {{<file "debian/changelog" >}} file.
* Once everybody is happy with the tarball, merge the release branch into the {{<file "master" >}} branch.
* Add a new tag on [Gitlab](https://gitlab.com/octopus-code/octopus/tags/new):
  * Create the new tag from the {{<file "master" >}} branch.
  * Tag should be named 10.0.
  * Add a link to {{<versioned-link "Releases/Octopus-10" "Octopus 10" >}} in the release notes.
  * The buildbot will automatically create from the tag a tarball (http://octopus-code.org/down.php?file=10.0/octopus-10.0.tar.gz), the variable documentation (http://octopus-code.org/doc/10.0/html/vars.php), and the doxygen documentation (http://octopus-code.org/doc/10.0/doxygen_doc/index.html).
* Merge the release branch into {{<file "develop" >}}. This might require to resolve some conflicts, as the {{<file "develop" >}} branch might have diverged.
* Update the codename for the development version in AC_INIT in {{<file "configure.ac" >}} on the {{< file "develop" >}} branch and on the {{<versioned-link "FAQ" "FAQ">}} page.
* Update the {{<file "data/releases.json" >}} file in the {{<octopus-documentation-git>}}
* Update the {{<file "default/content/Releases/Changes.md">}} file ({{<versioned-link "Releases/Changes">}}), referring to the merge requests on [Gitlab](https://gitlab.com/octopus-code/octopus/-/merge_requests?scope=all&utf8=✓&state=merged).
* Send an email announcement. <!---Sample e-mail below.--->
* Post it on the Octopus Facebook page! https://www.facebook.com/octopus.code
* Check and update {{<versioned-link "manual" "Manual" >}} and {{<versioned-link "tutorial" "Tutorials" >}}. (this takes time, it is better to do it after the rest of the release)

<!---
###  Sample email
```  
To: octopus-users@tddft.org, octopus-announce@tddft.org 

Subject: Octopus ''octopus_version'' released

Dear Octopus users,

We are pleased to announce that we have just released Octopus ''octopus_version''.

The source code, documentation and information for this new release can be obtained from:

http://octopus-code.org/wiki/Octopus_''octopus_version''

Best regards,

The Octopus development team
```
--->

##  Minor releases  

For a new minor release (_e.g._ 10.1):
* Create a hotfix branch from {{< file "master" >}}. The branch should be named {{< file "hotfix-10.1" >}}.
* Create a merge request on [Gitlab](https://gitlab.com/octopus-code/octopus/merge_request/new)  having the hotfix branch as source and the {{< file "master" >}} branch as target.
* Once all the bug fixes have been merged into the hotfix branch, update the version number in {{< code "AC_INIT" >}} in {{< file "configure.ac" >}}.
* Update the {{< file "PACKAGING" >}} file.
* Update the {{< file "debian/changelog" >}} file.
* Merge the hotfix branch into {{< file "master" >}}.
* Add a new tag on [Gitlab](https://gitlab.com/octopus-code/octopus/merge_requests/new):
  * Create the new tag from the {{< file "master" >}} branch.
  * Tag should be named 10.1.
  * The buildbot will automatically create from the tag a tarball (http://octopus-code.org/down.php?file=10.1/octopus-10.1.tar.gz), and the variable documentation (http://octopus-code.org/doc/10.1/html/vars.php).
* Create a new merge request on [Gitlab](https://gitlab.com/octopus-code/octopus/merge_requests/new) having the hotfix branch as source and the {{< file "develop" >}} branch as target.
* Merge the hotfix branch into {{< file "develop" >}}. There should be a conflict in the {{< file "configure.ac" >}} file, because of the changed version number. Keep the one coming from {{< file "develop" >}}. Other conflicts might occur.
* Update the {{<file "data/releases.json" >}} file in the {{<octopus-documentation-git>}}
* Update the {{<file "default/content/Releases/Changes.md">}} file ({{<versioned-link "Releases/Changes">}}), referring to the merge requests on [Gitlab](https://gitlab.com/octopus-code/octopus/-/merge_requests?scope=all&utf8=✓&state=merged).

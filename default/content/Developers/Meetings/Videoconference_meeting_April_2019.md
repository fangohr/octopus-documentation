---
title: "Videoconference April 2019"
weight: 18
---


This meeting will take place the 15 April at 18:00 CEST (16:00 UTC). 

Meeting URL: https://hangouts.google.com/call/aIH7NDZjCNvRRIqhi-yHAAEI 

###  Topics to discuss  
- Status of new release
- News from the test farm
- Handling of merge requests from forks
- Octopus paper
- News from real-time hybrids
- Octopus developers workshop

### Present  

Coming soon.

###  Meeting Minutes  

Coming soon.

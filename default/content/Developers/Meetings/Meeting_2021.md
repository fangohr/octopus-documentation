---
title: "Octopus Developers Meeting 2021"
section: "/home/luedersm/Downloads/Meeting_2021"
---


#  Octopus developers workshop - 29 September - 1 October 2021  
The fifth Octopus developers workshop will take place at the Max Planck Institute for the Dynamics and Structure of Matter, in Hamburg, Germany from September 29 to the October 1, 2021.

##  List of participants  
- Angel Rubio
- Micael Oliveira
- Heiko Appel
- Christian Schaefer
- Kevin Lively
- Hannes Huebener
- Hans Fangohr
- Sebastian Ohlmann
- Mukhtar Lawan
- Wenwen Mao
- Aaron Kelly
- Zahra Nourbakhsh
- Andrey Geondzhian
- I-Te Lu
- Davis  Welakuh Mbangheku
- Martin Lueders
- Franco Bonafé
- Esra Ilke Albar
- Sricharan RC
- Dongbin Shin
- John Bonini
- Johannes Flick
- Meisam Tabriz
- Nicolas Tancogne-Dejean
- Xavier Andrade
- Lukas Konecny
- Henning Glawe
- Matthieu Verstraete
- Johanna Fuks

##  Program  

|       | *Wednesday* | *Thursday* | *Friday* |
| :--:  |:---         |:----       |:----     |
| *Morning * | 09:00 Welcome                           | 09:00 Nicolas (DFT+U+V)    | 09:00 Xavier               |
|            | 09:10 Micael                            | 09:30 Sebastian (GPU)      | 09:45 Lukas                |
|            | 09:50 Martin                            | 09:50 Franco               | 10:10 Mukhtar              |
|            | 10:30 Coffee break                      | 10:30 Coffee break         | 10:30 Coffee break         |
|            | 11:00 Testsuite and testfarm discussion | 11:00 Ilke                 | 11:00 General discussions  |
|            | 12:00 Nicolas (magnons)                 | 11:15 Christian            | 12:30 Lunch                |
|            | 12:30 Lunch                             | 12:00 (Theory Group photo) |                            |
|            |                                         | 13:00 Lunch                |                            |
| *Afternoon* | 14:00 Nicolas (exciton)                | 14:30 Davis                |
|             | 14:40 Micael (multi-system)            | 15:10 Johannes             |
|             | 15:00 Multi-system discussion          | 15:50 John Bonini          |
|             | 16:00 Coffee break                     | 16:30 Coffee break         |
|             | 16:30 Johanna                          | 17:00 General discussions  |
|             | 17:00 General discussions              |                            |
| *Evening*   |  Workshop dinner | 


Talks:
- {{<versioned-link "presentations/meeting_2021/Octodev_2021_state_of_the_octopus.pdf" "State of the <del>Union</del>Octopus">}} (Micael Oliveira)
- New web and documentation system (Martin Lueders)
- Time-dependent magnons from first principles (Nicolas Tancogne-Dejean)
- Time-resolved Exciton wave functions from time-dependent density functional theory (Nicolas Tancogne-Dejean)
- {{<versioned-link "presentations/meeting_2021/Octodev_2021_multisystem.pdf" "Multi-system implementation in Octopus">}} (Micael Oliveira)
- Probing particle-particle correlation in harmonic traps with twisted light (Johanna Fuks)
- Effective electronic parameters from density-functional theory DFT+U and DFT+U+V (Nicolas Tancogne-Dejean)
- {{<versioned-link "presentations/meeting_2021/202109-octopus-workshop-gpu.pdf" "Octopus on GPU's">}} (Sebastian Ohlmann)
- Embracing the new multisystem framework: Maxwell, DFTB+ and linear media systems (Franco Bonafé)
- Archimedean Spirals and Twisted Light (Ilke Albar)
- Modified chemistry via strong light-matter coupling and light-matter coupling via radiation-reaction forces (Christian Schaefer)
- Linear-response methods for strongly coupled light-matter systems (Davis Welakuh)
- {{<versioned-link "presentations/meeting_2021/Octodev_2021_Octopus_and_strong_light-matter_coupling.pdf" "Octopus and strong light-matter coupling">}} (Johannes Flick)
- Ab initio linear-response approach to vibro-polaritons in the cavity Born-Oppenheimer approximation (John Bonini)
- INQ: a state-of-the art implementation of density functional theory for GPUs (Xavier Andrade)
- Relativistic electronic structure and core-level spectroscopies (Lukas Konecny)
- Simulation of Core levels Spectroscopy of Solids with Octopus (Mukhtar Lawan)

##  Discussions  

These are topics we should discuss during the meeting.

- Next Octopus developers workshop
- Update to coding standards
- Updates to the test-farm and test-suites
- Multi-system roadmap
- Status and next steps of the multi-resolution project


##  Practical information  
The meeting will be at the [https://goo.gl/maps/pVDNKmmgEDK2 MPSD] in Seminar Rooms SR I-III, except on Thursday, 9:00 to 15:00, were it will take place in Seminar rooms IV-V. You can find information how to reach it [https://www.mpsd.mpg.de/institute/contact/directions here].

---
title: "Videoconference February 2019"
weight: 17
---


This meeting will take place the 4 February at 18:00 CET. 

Meeting URL: https://hangouts.google.com/call/aIH7NDZjCNvRRIqhi-yHAAEI 

###  Topics to discuss  
- New release.
- Testsuite visualization app.
- Octopus Developers Workshop
- Automatic spacing.
- New Octopus paper
- GPU
- Mini-app
- isf vs libisf
- LDA+U, PCM code

###  Present  

Alberto, Ask, Hannes, Jose Rui, Martin, Matthieu, Micael, Miguel, Nicolas, Sebastian, Shunsuke, Xavier

###  Meeting Minutes  

- New release:
  - Micael: aim for early March as there are no big new features.
    - Ask: would like to finish interface to libvdwxc for the release.
  - Nicolas and Ask: some more tests for solids are needed.
  - Discussion about how to handle vdW functionals:
    - Xavier: interfaces are a bit irregular between vdWxc, Tkatchenko-Scheffler, etc. (Either as correction or as complete functional). Suggests unified treatment for all
    - Ask: fix either by new design or add lines to documentations.
    - Miguel: Allow xc description to have 3rd part. x + c + correction
    - Ask: Add issue on gitlab for discussion. Now: xc functional as all others, just non-local.
  - Ask, Nicolas, Matthieu, Miguel: Discussion about whether to allow 'incompatible' functionals.
    - Agreement that warnings should be issued in such cases.
    - Ask will volunteer to do that and create issue with proposal (within a week).
  - Miguel: other issue is PBE for linear response (missing implementation of kernel).
    - Possible project for Martin.
  - Sebastian: like to finish corrected CG (marked for the release, fix! Not a new feature)

- Testsuite App of Sebastian:
  - Micael: no Feedback for Sebastian (so far).
  - Some tests still miss bugs:
    - Nicolas: Ground state polarised gives different results depending on use of MPI (example)
    - Sebastian: some tests are too 'relaxed'
    - Micael: curvilinear coords test differs for OpenMP and no OpenMP.
  - Micael: due to better scripts, we could implement a test suite metatest. Could issue a warning for 'meaningless' tests.
  - Recent example, all tests but one gave similar results.
  - Graphical representation of results will help.
  - Xavier: We should look at it before release.
  - Xavier: Sebastian could make a video of how to use the test app.
  - Micael: Then we can dig into the unnoticed problems.

- Octopus developers workshop:
  - Where: Hamburg?
  - When? March no good,
  - Miguel suggests combining with Hardy's event. (June 13, Thursday)
  - Xavier supports.
  - Need to ask Angel.

- Automatic Spacing:
  - Xavier sums up:
    - So far: default spacing.
    - New idea: generate E vs spacing curve for each pseudopod and store with pseudopotential.
    - Then given accuracy can determine the spacing (and radius) Use those as default.
  - Micael: side-note on making the pseudo-dojo pseudopotentials the default set: forces are incorrect with non-linear core-corrections.
  - Micael: Does not like idea, because total E is not main feature of the code, and convergence for excited states can be very different wrt to spacing and radius.
  - Xavier: excited states allow for larger spacing and convergence should be safe.
  - Micael: user still wants optimal speed. For spacing he can agree. For radius it is more difficult. Suggests that user has to specify numbers explicitly.
  - Miguel: compare with VASP; specify precision goal.
  - Xavier: spacing could be a starting point and we can learn from it.
  - Nicolas: how reliable are the numbers from total energies in atomic case?
  - Micael: This is a question about responsibility. The problem is to give the user the wrong impression of safety.
  - Nicolas: we should not provide 'optimal values' if they are possibly not optimal.
  - Miguel: Shall we trust more the users of developers? No problem over-specifying the parameters.
  - Xavier and Miguel: users are usually lazy to run convergence tests.
  - Micael: no code provides automatic k-meshes.
  - Matthieu: what we want is a precision level, not a default number.
    - We should factor in different run modes and systems.
    - We could make a big table with default values for various precision goals (low, middle, high).
    - Limited use for atomic data and tests.
    - Agrees with Miguels about automatic system versus hard coded defaults.
    - Suggests run modes such as gs-high, gs-low, td-high, ...
  - Miguel: reminds on egg box effect.
  - Nicolas: automatic spacing is dangerous for connected calculations, such as gs -> td. Parameters should correspond to the final quantity to calculate.
  - Matthieu: VASP has exactly the same problem.
  - Micael: If you want to make users happier, automatic spacing is not the way. Make the GS more robust.
  - Xavier: Micael said to remove the code.
  - Micael: should not be removed, but discussed before being merged.
  - Discussion about what to discuss and how to decide:
    - Miguel proposed vote.
    - Micael suggests to break down into separate issues: spacing, radius.
  - Default values: Users might ask why that value...
  - Xavier: automatic spacing should be added to the code as experimental.
    - Discussion comparing automatic spacing with LDA+U (about experimental features).
    - Miguel: only problematic issue: numerical values for accuracy goals. Also problematic are radii.
    - Micael agrees with adding as 'experimental feature'.
  - Needs to be discussion again.
  - Matthieu about Abinit: there is variable (mostly unused) "accuracy" depending on run type and pseudopotential.
  - General feeling: names are better than values.
  - Compromise: experimental feature with 'high/med/low" accuracy goals.

- Next Octopus paper:
  - Micael states criteria for who should be author: new features not described in the last paper and things which are in public version at the time of publication.
  - List of topics:
    - solids
    - van der Waals (3 flavours)
    - Sternheimer for magneto optics
    - PCM (solvent stuff)
    - DFT+U
    - RDMFT (dressed RDMFT)
    - new propagators
    - local domains
    - coupling Maxwell equations to TD-KS
    - Photon OEP (photon contributions via Casida)
    - TSurff
    - technical section (parallelisation, test suite, etc.)
    - conductivity, thermal conductivity (Xavier)
    - GPU in parallel (try to run on Sierra) (Xavier)

- GPU
  - Micael: should have meeting dedicated to GPUs, efforts need to be coordinated
  - Xavier: need a GPU buildbot
  - Micael: Have two GPU cards but no machine to put them in
  - Xavier: GPU code is currently broken, Xavier trying to fix.

- Octopus Mini-App
  - Xavier: Mini version of the code to give to supercomputing centres to work on. Will be new branch with many things removed.
  - Nicolas: how to maintain it? What about splitting into libraries instead?
  - Xavier: libraries are much more work.
  - Micael: follow the development and see how it goes.

- isf vs libisf:
  - Xavier: not clear what are the differences between libisf and isf. If they do the same we should remove one of them.
  - Micael: libisf has been renamed PSolver and is now an independent tar-ball from BigDFT.
  - Nicolas: PSolver is difficult to use.
  - Micael: we should also remove libyaml, but Yaml is used in ISF. Which isf is faster, internal or library?
  - Xavier: in the long term drop both isf and libisf and implement the isf kernel directly in Octopus.
  - Nicolas: think about using tricks as in plane wave codes. Psolver also adds possibility of dielectric functions not equal one. Questions is whether we want to incorporate Psolver. We should not support libisf anymore.
  - Sebastian: libisf and psolver are comparable in performance
  - Micael: We should keep the option psolver. Psolver changed the interface, and currently cannot be used in the code.
  - Nicolas: split the issue in two parts: 1) remove libisf 2) include psolver.

- LDA+U, PCM code
  - Xavier: some of the LDA+U and PCM code is ugly. Should be better integrated.
  - Nicolas agrees but has no time to do it.
  - Xavier: example: some LDA+U_level in Force category.
  - Micael: sometimes ugly things are not easy to fix.

- Heads up on for some upcoming changes
  - Micael: Add performance regression test to build bot. Tricky thing is how to measure performance.
    - Xavier: need new tests to measure performance.
  - Multi-system refactoring has become priority in Hamburg. Want to start in April, after release.
  - Break it up as much as possible, Need more people to review the code.

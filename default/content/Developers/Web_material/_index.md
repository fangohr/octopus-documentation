---
Title: "Web material"
Weight: 1
Description: "How to write tutorials, manuals, etc."
---

This section is concerned with the {{< octopus >}} web pages. Read here is you want to contribute to the manual or the tutorials.

{{% children depth=1 /%}}

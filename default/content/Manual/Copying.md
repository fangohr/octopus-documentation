---
title: "Copying"
section: "Manual"
---


###  INTRODUCTION  

The real-space TDDFT code octopus ("octopus") is provided under the 
GNU General Public License ("GPL"), Version 2, with exceptions for 
external libraries that are contained solely for convenience in this 
distribution. 

You can find a copy of the GPL license [http://www.gnu.org/copyleft/gpl.html  here. ]

A copy of the exceptions and licenses follow this introduction.

###  LICENSE EXCEPTIONS  

Octopus provides several external libraries, which are located in the 
"external_libs" subdirectory of the octopus source distribution. The 
GNU General Public License does not apply to these libraries. Separate 
copyright notices can be found for each library in the respective 
subdirectories of "external_libs". Copyright notices are also contained 
in this document.

Currently the following external libraries are provided:

###  Expokit  
* Roger B. Sidje
* Department of Mathematics, University of Queensland 
* Brisbane, QLD-4072, Australia
* Email: rbs@maths.uq.edu.au
* WWW: http://www.maths.uq.edu.au/expokit/ 
* Copyright: http://www.maths.uq.edu.au/expokit/copyright

###  Metis 4.0  

* George Karypis 
* Department of Computer Science & Engineering
* Twin Cities Campus
* University of Minnesota, Minneapolis, MN, USA
* Email: karypis@cs.umn.edu
* WWW: http://www-users.cs.umn.edu/~karypis/metis/index.html

####  METIS COPYRIGHT NOTICE  

The ParMETIS/METIS package is copyrighted by the Regents of the
University of Minnesota. It can be freely used for educational and
research purposes by non-profit institutions and US government agencies
only.  Other organizations are allowed to use ParMETIS/METIS only for
evaluation purposes, and any further uses will require prior approval.
The software may not be sold or redistributed without prior approval.
One may make copies of the software for their use provided that the
copies, are not sold or distributed, are used under the same terms
and conditions.

As unestablished research software, this code is provided on an
``as is'' basis without warranty of any kind, either expressed or
implied. The downloading, or executing any part of this software
constitutes an implicit agreement to these terms. These terms and
conditions are subject to change at any time without prior notice.

###  qshep  

*Robert Renka
*University of North Texas
*(817) 565-2767


####  QSHEP COPYRIGHT NOTICE  

ACM Software Copyright Notice

[http://www.acm.org/pubs/toc/CRnotice.html]

Copyright   (c)  1998  Association   for  Computing   Machinery,  Inc.
Permission to  include in application  software or to make  digital or
hard copies  of part or all of  this work is subject  to the following
licensing agreement.

ACM Software License Agreement

All software, both binary and  source published by the Association for
Computing  Machinery  (hereafter,  Software)  is  copyrighted  by  the
Association  (hereafter, ACM) and  ownership of  all right,  title and
interest in and to the Software remains with ACM.  By using or copying
the Software, User agrees to abide by the terms of this Agreement.

Noncommercial Use

The ACM  grants to you (hereafter, User)  a royalty-free, nonexclusive
right  to execute,  copy, modify  and distribute  both the  binary and
source  code   solely  for   academic,  research  and   other  similar
noncommercial uses, subject to the following conditions:

1. User  acknowledges that the  Software is  still in  the development
stage  and that  it is  being supplied  "as is,"  without  any support
services   from  ACM.    Neither  ACM   nor  the   author   makes  any
representations or warranties,  express or implied, including, without
limitation, any  representations or warranties  of the merchantability
or fitness for any particular  purpose, or that the application of the
software, will not infringe on any patents or other proprietary rights
of others.

2. ACM shall  not be held  liable for direct, indirect,  incidental or
consequential  damages arising  from any  claim by  User or  any third
party with respect  to uses allowed under this  Agreement, or from any
use of the Software.

3. User agrees  to fully  indemnify and hold  harmless ACM  and/or the
author(s) of  the original work from  and against any  and all claims,
demands, suits, losses, damages, costs and expenses arising out of the
User's use of the Software, including, without limitation, arising out
of the User's modification of the Software.

4. User may modify  the Software and distribute that  modified work to
third  parties provided  that: (a)  if posted  separately,  it clearly
acknowledges  that it  contains  material copyrighted  by  ACM (b)  no
charge is associated  with such copies, (c) User  agrees to notify ACM
and the Author(s)  of the distribution, and (d)  User clearly notifies
secondary users that such modified work is not the original Software.

5. User agrees that  ACM, the authors of the  original work and others
may enjoy  a royalty-free, non-exclusive license to  use, copy, modify
and redistribute these modifications to  the Software made by the User
and  distributed to  third parties  as  a derivative  work under  this
agreement.

6. This agreement will terminate immediately upon User's breach of, or
non-compliance with,  any of its terms.   User may be  held liable for
any  copyright   infringement  or   the  infringement  of   any  other
proprietary rights  in the Software  that is caused or  facilitated by
the User's failure to abide by the terms of this agreement.

7. This agreement  will be construed  and enforced in  accordance with
the law  of the  state of New  York applicable to  contracts performed
entirely  within the State.   The parties  irrevocably consent  to the
exclusive jurisdiction of  the state or federal courts  located in the
City of New York for all disputes concerning this agreement.

Commercial Use

Any User wishing to make a commercial use of the Software must contact
ACM  at   permissions@acm.org  to  arrange   an  appropriate  license.
Commercial use  includes (1) integrating or incorporating  all or part
of the source code into a product for sale or license by, or on behalf
of, User to third parties, or (2) distribution of the binary or source
code  to third  parties  for use  with  a commercial  product sold  or
licensed by, or on behalf of, User.

Revised 6/98

{{< manual-foot prev="Manual:Appendix:Reference Manual" next="Manual" >}}
---------------------------------------------

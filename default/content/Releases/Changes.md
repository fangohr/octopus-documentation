---
title: "Changes"
---


This page contains the changes in the releases of {{< octopus >}}.

### Octopus 14

#### 14.0 - 2023-03-12

* New features and functionality
  * Multisystem framework: 
    * introduce generic interaction counters {{<MR "2242">}} (closes {{<issue "634">}})
  * Maxwell Code:
    * Implement spatial envelope function from expression {{<MR "2346">}}
    * Add external EM source {{<MR "1981">}}
    * Add diamagnetic current density from non-uniform vector potential {{<MR "2043">}}
    * Output dipolar field in electrons {{<MR "2078">}}
    * Bessel waves: add envelope {{<MR "2266">}}, add shift of centee {{<MR "2188">}}
    * Helmholtz decomposition for non Coulomb gauge {{<MR "2234">}} (adresses {{<issue "705">}})
  * Stress tensor:
    * 2D Ewald stress {{<MR "2012">}}
    * GGA stress tensor.{{<MR "2008">}}
  * Photodoping approach (constrained DFT) {{<MR "2303">}}
  * Self-interaction corrections:
    * Non-collinear SIC {{<MR "2009">}}
    * Adding the possibility to use the SCDM method for localizing orbitals for PZ-SIC calculations. {{<MR "2256">}}
  * Fully relativistic UPF2 pseudopotentials {{<MR "2310">}}
  * Allow for running a kick in lower dimension than 3 {{<MR "2212">}}
  * Scale regridding quantities by their 2-norm {{<MR "2135">}}
  * Add nearest-neighbor interpolation for regridding {{<MR "2132">}}
  * Solution of SCF at the ZORA relativistic level of theory. {{<MR "2074">}} (closes {{<issue "605">}}), {{<MR "2145">}}
  * Introduce the possibility to shift the center of the simulation box {{<MR "2032">}} (closes {{<issue "699">}})
  * Enable the Chebyshev filtering eigensolver for States parallelization. {{<MR "2247">}}
  * QEDFT functional (Breit approximation) {{<MR "2292">}}
  * Add the support of DFT+U with spinors on GPUs {{<MR "2082">}}
  * External libraries:
    * Add support for psolver 1.9.2 {{<MR "2155">}} (closes {{<issue "764">}})
    * Update spglib: {{<MR "2205">}}, {{<MR "2203">}}, {{<MR "1913">}}
    * Update BerkeleyGW interface to BGW 3.0 {{<MR "2257">}}

* Bug fixes:
  * Fix GDLib support when spacing is not defined {{<MR "2198">}} (closes {{<issue "756">}})
  * Forbid to compute the dipole acceleration for mGGAs and hybrids, as there is a missing term {{<MR "2352">}} (see {{<issue "683">}})
  * Fix a bug with TDOuput=td_occup with TDProjStateStart > 1 {{<MR "2332">}} (closes {{<issue "533">}} and {{<issue "575">}})
  * Fix some problems with the option td_kpoint_occup and stops the code with the option is not supported {{<MR "2333">}} (closes {{<issue "441">}})
  * Fix a wrong label in the file. {{<MR "2330">}} (closes {{<issue "891">}})
  * Fix potential out-of-bound exception in the symmetrizer {{<MR "2295">}} (closes {{<issue "881">}})
  * Fix LCAO alternative with k-points and domain parallelization {{<MR "2296">}}
  * OEP/KLI:
    * Fix the OEP full calculations {{<MR "2104">}}
    * Fix the k-point index in KLI. {{<MR "2127">}}
  * Maxwell code:
    * Add check for incompatible time-step and interaction timing {{<MR "2285">}} (closes {{<issue "829">}})
    * Fix some Maxwell couplings with MPI {{<MR "2249">}} (closes {{<issue "793">}})
    * Fix sign of quadrupolar term and add test {{<MR "2179">}}
    * Fix factor for Maxwell vector potential {{<MR "2075">}}
    * Fix sign of dipolar vector potential for phase {{<MR "2362">}}
    * Allocate Mxll arrays and get fields from the interactions only if there is some kind of Maxwell coupling {{<MR "2334">}}
      (closes {{<issue "931">}})
    * Enable all EM fields from the external source {{<MR "2243">}} (closes {{<issue "837">}}, {{<issue "834">}})
    * Fix data dependency in hamiltonian_elec_update for mxll_coupling_calc {{<MR "2319">}}, {{<MR "2320">}} (closes {{<issue "793">}})
  * Bugfix for the calculation of the Coulomb integrals used to get the ab initio U. {{<MR "2276">}}
  * Fix a bug in the generation of the random vectors for Chebyshev {{<MR "2255">}} (closes {{<issue "862">}})
  * Forces/Stress tensor:
    * Fix the Ewald summation for forces/stress. {{<MR "2150">}}
    * Improve the quality of forces and fixes a bug for the calculation of the gradient on GPUs for non-orthogonal cells. {{<MR "2110">}}
  * Shorten some profiling  names such that they have less than 25 characters. {{<MR "2111">}} (closes {{<issue "732">}})
  * Forbids to run unsupported calculations. {{<MR "2072">}}
  * X(matrix_norm2) and X(singular_value_decomp) will now preserve the matrix by default {{<MR "2235">}} (closes {{<issue "721">}})
  * Fix uninitialized variables and memory leaks: {{<MR "2302">}}, {{<MR "2206">}}, {{<MR "2202">}}, {{<MR "2199">}}
  * Various bugfixes: {{<MR "2183">}}, {{<MR "2216">}}, {{<MR "2261">}}, {{<MR "2356">}}, {{<MR "2349">}}, {{<MR "2343">}},
    {{<MR "2158">}}, {{<MR "2286">}}, {{<MR "2288">}}, {{<MR "2204">}}, {{<MR "2245">}}, {{<MR "2213">}}, {{<MR "2168">}},
    {{<MR "2196">}}, {{<MR "2154">}}, {{<MR "2180">}}, {{<MR "2134">}}, {{<MR "2126">}}, {{<MR "2124">}}, {{<MR "2121">}},
    {{<MR "2098">}}, {{<MR "2103">}}, {{<MR "2086">}}, {{<MR "2092">}}, {{<MR "2081">}}, {{<MR "2080">}}, {{<MR "2066">}},
    {{<MR "2280">}}, {{<MR "2328">}}, {{<MR "2157">}}, {{<MR "2359">}}, {{<MR "2331">}} (closes {{<issue "889">}}),
    {{<MR "2201">}}, {{<MR "2193">}}

* Removed or deprecated features:
  * Remove LIBISF Support {{<MR "2353">}} {{<MR "2306">}}
  * Mark FMM as deprecated {{<MR "2342">}}
  * Remove support for libxc4 {{<MR "2328">}} (closes {{<issue "717">}})
  * Remove the eigensolver cg_new which is not maintained and deprecated {{<MR "1861">}}
  * For periodic dimensions we now remove the averaged force, as down in other DFT codes for solids. {{<MR "2151">}}

* Optimization:
  * Initialize batches for np points {{<MR "2211">}} (closes {{<issue "95">}})
  * Reduce temp array creation warnings  {{<MR "2237">}}, {{<MR "2228">}}
  * All-to-all exchange of batches {{<MR "2264">}}
  * Several optimization for vdw TS forces. {{<MR "2315">}}
  * Use phase correction {{<MR "1374">}}
  * Optimization of the calculation of DFT+U forces. {{<MR "2083">}}
  * Improvements for the Chebyshev filtering {{<MR "2077">}}
  * Optimized hamiltonian_elec_base_nlocal_force {{<MR "2220">}} (closes {{<issue "87">}})
  * Other optimizations: {{<MR "2176">}}, {{<MR "2300">}}, {{<MR "2147">}}, {{<MR "2174">}}, {{<MR "2131">}}, {{<MR "2326">}}


* Other changes:
  * Increases the number of k-points and states that can be written for the wavefunction output. {{<MR "2312">}}
  * Update atomic masses in element.dat {{<MR "2221">}} (closes {{<issue "658">}})
  * Forbid to call MPI inside OpenMP regions. {{<MR "2252">}}
  * Catch MPI errors {{<MR "2248">}}
  * Improved stability for KLI equation.{{<MR "2167">}}
  * Improved stability for LSDA with spinors. {{<MR "2175">}}
  * Improve on-demand quantities and make the current an on-demand quantity  {{<MR "2045">}}
  * Improve ExtraStatesInPercent: round up  {{<MR "2100">}}
  * Minor improvements {{<MR "2062">}}
  * Add options (cell_shape and cell_colume) for upcoming geometry optimization {{<MR "2108">}}

* Documentation and tutorials:
  * Updating tutorials: {{<MR "2116">}}, {{<MR "2209">}}, {{<MR "2323">}}
  * Improving documentation: {{<MR "2233">}} (addresses {{<issue "810">}} and {{<issue "813">}}), 
    {{<MR "2225">}} (addresses {{<issue "655">}}), {{<MR "2329">}} (closes {{<issue "777">}} and {{<issue "786">}}), {{<MR "2254">}}, 
    {{<MR "2231">}}, {{<MR "2190">}}, {{<MR "2313">}}, {{<MR "2034">}} (closing {{<issue "701">}})
  * Website updates: {{<MR "2339">}} (closes {{<issue "936">}}), {{<MR "2229">}} (closes {{<issue "787">}}), {{<MR "2351">}} (closes {{<issue "953">}}), {{<MR "2311">}}

* Build system:
  * CMake updates: {{<MR "2169">}}, {{<MR "2299">}}, {{<MR "2297">}} (closes {{<issue "642">}} and {{<issue "865">}}), {{<MR "2324">}}
  * Autotools updates: {{<MR "2236">}}, {{<MR "2215">}}, {{<MR "2289">}}, {{<MR "2294">}}, {{<MR "2200">}}
  * Various updates: {{<MR "2241">}}, {{<MR "2293">}}, {{<MR "1925">}}, {{<MR "2360">}}
  * Updates for MacOS: {{<MR "2050">}}

* CI, Testsuite, Coding standards:
  * Add new tests: {{<MR "2223">}} (closes {{<issue "118">}})
  * Shorten some tests: {{<MR "2274">}}, {{<MR "2107">}}, {{<MR "2272">}}, {{<MR "2253">}}
  * Update for libxc6: {{<MR "2251">}}, {{<MR "2278">}}
  * Update tolerances: {{<MR "2279">}}, {{<MR "2267">}}, {{<MR "2271">}}, {{<MR "2287">}}, {{<MR "2317">}}, {{<MR "2316">}},
    {{<MR "2191">}}, {{<MR "2327">}}, {{<MR "2290">}}, {{<MR "2263">}}, {{<MR "2240">}}, {{<MR "2207">}}, {{<MR "2194">}}
  * Activate tests on GPU {{<MR "2181">}}
  * Improve some tests: {{<MR "2119">}}, {{<MR "2318">}} (closes {{<issue "845">}}), {{<MR "2259">}}
  * Improve test script {{<MR "2195">}}, {{<MR "2291">}}
  * Capture runtime warnings: {{<MR "2184">}}, {{<MR "2277">}}, {{<MR "2187">}}, {{<MR "2189">}}

* Refactoring and code cleanup:
  * Remove calc_physical_current routine {{<MR "2210">}} (closes {{<issue "66">}})
  * Address compiler warnings {{<MR "2114">}}, {{<MR "2265">}}, {{<MR "2258">}}, {{<MR "2224">}}, {{<MR "2219">}}
  * Remove macros: MAX_DIM {{<MR "2153">}}, CNST {{<MR "2268">}}, REAL_\* {{<MR "2182">}}
  * Introduces a class for handling ring-pattern communications {{<MR "2262">}}
  * Refactoring for the multisystem framework: {{<MR "2301">}}, {{<MR "2185">}}, {{<MR "2170">}}, {{<MR "2177">}}, {{<MR "2028">}},
    {{<MR "2156">}}, {{<MR "2106">}}, {{<MR "1391">}}, {{<MR "2269">}}, {{<MR "2230">}} (closes {{<issue "811">}}),
    {{<MR "1911">}}
  * Clean up modules {{<MR "1442">}} (closes {{<issue "880">}}), {{<MR "2162">}}, {{<MR "2102">}}, {{<MR "2099">}}, {{<MR "2097">}}
  * Various code cleaning: {{<MR "2282">}}, {{<MR "2218">}} (closes {{<issue "127">}}),  {{<MR "2232">}} (closes {{<issue "825">}}),
    {{<MR "2105">}}, {{<MR "2226">}}, {{<MR "2284">}}, {{<MR "2307">}}


### Octopus 13

#### 13.0 - 2023-06-28


* New features and functionality: 
  * Chebychev filtering {{<MR "2042">}}, {{<MR "2014">}}, {{<MR "1953">}}
  * Implements Born-Oppenheimer Molecular Dynamics in the multisystem framework {{<MR "2022">}}
  * Introduce a generic field transfer interaction that includes interpolation {{<MR "2017">}}
  * Add diamagnetic and magnetization current densities {{<MR "2011">}} (closes {{<issue "584">}})
  * Add the support of k-point symmetries for the stress tensor. {{<MR "2007">}}
  * Adds the NLCC contribution to the stress tensor. {{<MR "2006">}}
  * Forbids to compute the stress tensor in unsupported cases. {{<MR "1985">}}
  * Improve multisystem propagation {{<MR "1983">}}
  * Extra states as percentage of the amount of occupied states {{<MR "1965">}} (closes {{<issue "661">}})
  * Implements the Analytical Norm-Conserving (ANC) regularized potential of Gygi. {{<MR "1946">}}
  * Allow the time-imaginary evolution eigensolver to run in states parallel. {{<MR "1824">}}
  * oct-wannier90 now supports k-point parallelization for the outputs. {{<MR "1800">}}
  * Helmholtz Decomposition surface correction. {{<MR "1791">}}
  * Implement PML for Maxwell's equations on GPUs. {{<MR "1766">}}
  * Improve transfers {{<MR "1743">}}, {{<MR "1739">}} 
  * Enable interactions for electrons and Maxwell (Maxwell-TDDFT forward-backward coupling) {{<MR "1738">}}
  * Implement spaced-dependent magnetic-field to Zeeman_potential. {{<MR "1731">}} (closes {{<issue "572">}})
  * Add a new box type based on files interpreted with cgal. {{<MR "1728">}}
  * Allows to construct more than one localized subspace from states (DFTUBasisFromStates), for DFT+U calculations. {{<MR "1691">}}
  * Implementation of the KLI approximation with k-points. {{<MR "1585">}}

* Bug fixes:
  * Fixes the commutator [r,Vnl] on GPUs for SOC. {{<MR "2055">}} (closes {{<issue "714">}})
  * Fixes some problems with HF and hybrid functionals with k-points {{<MR "2023">}}
  * Fix for range-separated hybrid functionals for isolated systems {{<MR "2003">}}
  * Bugfixes for guess the atomic occupations for HGH pseudopotentials. {{<MR "1980">}}
  * Bugfix for scalapack {{<MR "1972">}}
  * This MR fixes the initialization of spec%sigma for Species Full Gaussian. {{<MR "1971">}} (closes {{<issue "668">}})
  * Fixes a too long profiler name. {{<MR "1970">}} (closes {{<issue "665">}})
  * Fixes the index for getting the principal quantum number in all-electron calculations. {{<MR "1969">}}
  * Prevent accessing the index 1 in the array if the size is zero. {{<MR "1959">}}
  * Fixes the randomization for domain parallelization {{<MR "1956">}}
  * Fixes the parsing of PBD coordinate files {{<MR "1954">}} (closes {{<issue "653">}})
  * Fix the indices for curvilinear meshes. {{<MR "1950">}}
  * Fixes several problems with all-electron species. {{<MR "1949">}}
  * Improves the stability of the root finding for species full Gaussian {{<MR "1943">}} (closes {{<issue "647">}})
  * Remove explicit initialization in many functions {{<MR "1937">}}
  * Fixes SOC on GPUs {{<MR "1933">}} (closes {{<issue "607">}})
  * This checks if a static electric field will break the symmetries or not of the system. {{<MR "1910">}} (closes {{<issue "424">}})
  * Fix a bug for the magnon kick on GPUs. {{<MR "1905">}}
  * Bugfix: One body matrix elements for Hartree-Fock. {{<MR "1890">}}
  * Resuscitate OpenCL interface. {{<MR "1863">}} (closes {{<issue "566">}})
  * Fix GPU routine for integers. {{<MR "1862">}}
  * Fixing Octopus internally-defined functionals to avoid conflicts with libxc. {{<MR "1852">}} (closes {{<issue "600">}})
  * Fix the vtk output for vectors in parallel. {{<MR "1838">}}
  * Bugfix for kdotp run mode. {{<MR "1817">}}
  * Fix LCAO negative atomic densities. {{<MR "1807">}}
  * Improve the numerical stability of the Broyden mixing. {{<MR "1780">}}
  * Bugfix for ADSIC in the unpolarized case. ADSIC now properly computes the energy. {{<MR "1778">}}
  * Fix a problem of nested calls to the same profiler in the case of multigrid on GPU. {{<MR "1773">}}
  * Enable GS runs for spinors on GPUs. {{<MR "1754">}}
  * Bugfix for Hartree-Fock and full-range hybrids with k-points. {{<MR "1662">}}
  * Minor bugfixes: {{<MR "2058">}}, {{<MR "2057">}}, {{<MR "2051">}}, {{<MR "2046">}}, {{<MR "2040">}}, {{<MR "2024">}}, {{<MR "2018">}},
    {{<MR "2013">}}, {{<MR "1976">}}, {{<MR "1963">}} (closes {{<issue "657">}}), {{<MR "1958">}}, {{<MR "1936">}} (closes {{<issue "646">}}),
    {{<MR "1935">}}, {{<MR "1932">}}, {{<MR "1930">}} (closes {{<issue "630">}}), {{<MR "1929">}} (closes {{<issue "631">}}), 
    {{<MR "1928">}}, {{<MR "1923">}} (closes {{<issue "396">}}), {{<MR "1917">}}, {{<MR "1899">}}, {{<MR "1875">}}, {{<MR "1859">}},
    {{<MR "1858">}}, {{<MR "1850">}}, {{<MR "1844">}}, {{<MR "1837">}}, {{<MR "1833">}}, {{<MR "1812">}}, {{<MR "1765">}},
    {{<MR "1763">}}, {{<MR "1761">}}, {{<MR "1727">}}, {{<MR "1679">}}

* Optimization:
  * Optimization of the calculation of the stress tensor {{<MR "1966">}} (closes {{<issue "660">}})
  * Optimization of the magnetic term. {{<MR "1924">}}
  * Spin orbit coupling on GPU {{<MR "1760">}}
  * Improved GPU performances. {{<MR "1876">}}, {{<MR "1872">}}, {{<MR "1869">}}, {{<MR "1867">}}, {{<MR "1795">}}, {{<MR "1789">}},
    {{<MR "1781">}}
  * OpenMP optimization. {{<MR "1866">}}, {{<MR "1784">}}
  * Make the mesh_to_cube mapping local. {{<MR "1707">}}
  * Adding the GPU support for DFT+U. {{<MR "1704">}}
  * Allow systems with self-overlapping projectors to run on GPUs {{<MR "1698">}} (closes {{<issue "564">}})
  * Various optimizations: {{<MR "1960">}}, {{<MR "1854">}}, {{<MR "1832">}}, {{<MR "1772">}}, {{<MR "1757">}}, {{<MR "1693">}}

* Refactoring and code cleanup:
  * Reduce dependencies on the ions_m module {{<MR "2030">}}
  * Split the STEP_DONE algorithmic operation in two {{<MR "2029">}}
  * Invert dependency between system and propagator {{<MR "2021">}}
  * Remove ions dependency from restart {{<MR "2020">}}
  * Introduce exec_end_of_timestep_tasks for common tasks in the Maxwell system  {{<MR "2016">}}
  * Improved Maxwell propagation; new Leapfrog propagator {{<MR "2010">}}
  * Rewrites the long-range term of the stress tensor for improved efficiency and accuracy. {{<MR "2000">}}
  * Split the pseudopotential contribution into a local and a nonlocal part. {{<MR "1994">}}
  * Change how the multisystem framework determines if an algorithm is finished {{<MR "1991">}}
  * Change the way algorithmic operations are done  {{<MR "1990">}}
  * Refactors the Ewald stress tensor term {{<MR "1986">}}
  * Refactor TD initialization {{<MR "1977">}}
  * Improves the calculation of the Hartree contribution to the stress tensor. {{<MR "1974">}} (closes {{<issue "667">}})
  * Aetrs propagator in the new framework {{<MR "1968">}}
  * Remove some unnecessary dependencies in the multigrid module. {{<MR "1951">}}
  * Simplify logic for td_interpolate {{<MR "1941">}}
  * Introduce algorithm factories {{<MR "1909">}}
  * Some refactoring of the output module. {{<MR "1868">}}, {{<MR "1764">}}
  * refactor stress calculations {{<MR "1857">}} (closes {{<issue "281">}})
  * Change handling of quantities in multisystem framework to correctly handle static quantities. {{<MR "1851">}}
  * Ions refactoring {{<MR "1849">}}
  * Symmetrizer to grid {{<MR "1847">}}
  * Refactoring of the perturbations in terms of classes and trying to beautify the code. {{<MR "1816">}}
  * Refactoring the PZ SIC and trying to improve the logic of the code in few places. {{<MR "1782">}}
  * Forbid to call batch operations within OpenMP regions, at least the ones using explicitly OpenMP. {{<MR "1745">}}
  * Adding the support of current_to_maxwell interaction for charged particles. {{<MR "1824">}}
  * Making grid a child class of mesh. {{<MR "1587">}}
  * Various refactoring: {{<MR "1776">}}, {{<MR "1737">}}, {{<MR "1732">}}, {{<MR "1729">}}
  * Cleanup {{<MR "2001">}}, {{<MR "1997">}}, {{<MR "1843">}}, {{<MR "1821">}}, {{<MR "1805">}}, {{<MR "1680">}}


* Removed or deprecated features:
  * Marked cg_new as deprecated {{<MR "2056">}}
  * Removes the QM/MM features {{<MR "2957">}} (closes {{<issue "653">}})

* Other changes:
  * This MR increases the default value of the Gaussian width for species_full_gaussian {{<MR "1979">}} (closes {{<issue "676">}})
  * Make electronic system run properly in the multisystem framework {{<MR "1978">}}
  * Allow for Longer Path Names {{<MR "1967">}} (closes {{<issue "666">}})
  * Improved support of all-electron calculations. {{<MR "1962">}}
  * Add memory profiling to batch allocations. {{<MR "1897">}}
  * Change the buffer to be from single precision to be double precision. {{<MR "1889">}}
  * Do not allow FFTW grids that are not compatible with CUFFT ones. {{<MR "1887">}}
  * Forbid to run fully relativistic UPF2. {{<MR "1886">}}
  * In order to comply with the latest versions of libxc, lda_x_1d is renamed as lda_x_1d_soft. {{<MR "1846">}}
  * Remove unused EigensolverSkipKpoints input variable. {{<MR "1829">}}
  * Remove the stringent pseudodojo sets. {{<MR "1830">}}
  * Updating the SG15 pseudopotential set to the 1.2 version. {{<MR "1825">}}
  * Forbid to call batch operations within OpenMP regions, at least the ones using explicitly OpenMP. {{<MR "1750">}}
  * Minor changes: {{<MR "1952">}}, {{<MR "1907">}}, {{<MR "1839">}}, {{<MR "1828">}}, {{<MR "1811">}}, {{<MR "1809">}}, 
    {{<MR "1806">}}, {{<MR "1767">}}, {{<MR "1759">}}, {{<MR "1756">}}, {{<MR "1751">}}, {{<MR "1726">}}, {{<MR "1602">}},
    {{<MR "1550">}}, {{<MR "1484">}}

* Documentation and tutorials:
  * add hybrid tutorial {{<MR "1993">}}
  * tutorial updates: {{<MR "2070">}}, {{<MR "2069">}}
  * Doxygen documentation updates: {{<MR "2033">}}, {{<MR "1989">}}, {{<MR "1988">}}
  * add page on Octopus features {{<MR "1860">}}
  * Various documentation updates: {{<MR "2035">}}, {{<MR "2031">}}, {{<MR "2002">}}, {{<MR "1999">}}, {{<MR "1996">}}, 
    {{<MR "1947">}}, {{<MR "1945">}}, {{<MR "1939">}}, {{<MR "1808">}}, {{<MR "1770">}}

* CI, Testsuite, Coding standards:
  * Various updates: {{<MR "1927">}}, {{<MR "1918">}}, {{<MR "1916">}}, {{<MR "1915">}}, {{<MR "1908">}}, {{<MR "1906">}}, 
    {{<MR "1904">}}, {{<MR "1894">}}, {{<MR "1895">}}, {{<MR "1891">}}, {{<MR "1883">}}, {{<MR "1845">}}, {{<MR "1810">}},
    {{<MR "1799">}}, {{<MR "1786">}}, {{<MR "1755">}}
  * Run pre-commit: {{<MR "1902">}}, {{<MR "1901">}}, {{<MR "1893">}}, {{<MR "1831">}}
  * Buildsystem: {{<MR "1836">}}, {{<MR "1826">}}, {{<MR "1752">}}, {{<MR "1736">}}


### Octopus 12

#### 12.2 - 2023-02-14

* Bug fixes:
  * Fix uninitialized variable (residue_previous). {{<MR "1885">}}
  * The OEP full was not taking the right occupations for the spin-polarized case. {{<MR "1856">}}
  * Fix the restart of DFT+U. {{<MR "1864">}}
  * Fixes the output option energy_density with MGGA functional having an energy-dependence. {{<MR "1841">}} (resolves {{<issue "596">}})
  * Fix GPU bug for domain parallelization. {{<MR "1835">}}
  * Fix memory leak for the multigrid preconditioner. {{<MR "1814">}}

#### 12.1 - 2022-10-14

* Bug fixes:
  * Minor bugfixes for the current computed with DFT+U(+V) {{<MR "1785">}}
  * Bugfix that was presenting to use the option TDFreezeOrbitals=sae. {{<MR "1792">}}
  * Bugfixes for LCAO for metallic systems and for k-point parallelization. {{<MR "1801">}}
* Documentation and tutorials
  * Update tutorials for periodic systems. {{<MR "1788">}}
  * Update model system tutorials. {{<MR "1797">}}
  * Update HPC tutorials. {{<MR "1798">}}
  * Update Maxwell tutorials. {{<MR "1804">}}
  * Add MPXCD compilation script. {{<MR "1752">}}
  * Various updates to the documetation. {{<MR "1802">}}

#### 12.0 - 2022-09-16

* New features and functionality
  * Implementation of constrained DFT for noncollinear magnetism. {{<MR "1499">}}
  * The output kanamoriU is now allowed for spinors. {{<MR "1507">}}
  * making external photon mode file also available for time-dependent photon runs {{<MR "1489">}}
  * Enable writing linear medium's points, permittivity and permeability to output. {{<MR "1421">}} (resolves {{<issue "469">}})
  * Implementation of constrained DFT for noncollinear magnetism. {{<MR "1499">}}
  * Add restart functionality to the multisystem framework {{<MR "1539">}} (closes {{<issue "332">}})
  * Add the support of VTK output for Brillouin-zone resolved outputs. {{<MR "1514">}}
  * Computing the exchange-virial expression for exchange-only DFT calculations. {{<MR "1509">}}
  * Add colors to NVTX ranges {{<MR "1555">}}
  * The intersite interaction can now also be computed using 'DFTUBasisFromStates = yes'. {{<MR "1388">}}
  * Removed option to use default a spacing and a default radius. {{<MR "1567">}}
  * Enable Maxwell restart {{<MR "1535">}}
  * Use XCKernelLRCAlpha to specify the value of Alpha. (-\Alpha/q^2) {{<MR "1488">}}
  * An Hartree-Fock calculation now computes and prints the exact exchange energy in the static/info file. {{<MR "1496">}}
  * Completely distributed generation of indices {{<MR "1583">}}
  * Allow to define the laser polarization in terms of Miller indices. {{<MR "1608">}}
  * Adding the support of the commutator of the position operator and the +U+V nonlocal operator for spinors. {{<MR "1513">}}
  * Add the possibility to print k-point resolved eigenvalues. {{<MR "1606">}}
  * Introducing a new utility, oct-tdtdm, to compute the time dependent TDM from the td_occup output of Octopus. {{<MR "1274">}}
  * Adding the support of the treatment of the Coulomb singularity in 1D. {{<MR "1645">}}
  * Improving the guess density for LCAO with species_full_delta. {{<MR "1658">}}
  * Enable imaginary absorbing boundaries on GPUs {{<MR "1643">}}
  * Enable large grids with more than about 1 billion points {{<MR "1653">}}
  * Introducing a new variable to define the momentum transfered in reduced coordinates. {{<MR "1664">}}
  * Adding a variable to control the tolerance of the symmetry finder. {{<MR "1677">}}
  * Implement dispersive materials as Maxwell linear media  {{<MR "1482">}}
  * Allow phases on GPU {{<MR "1676">}}
  * CG improvements to reach very accurate convergence {{<MR "1176">}}

* Bugfixes
  * Correction to ztmp allocation {{<MR "1450">}}
  * Fix lambda normalization with multiple modes {{<MR "1497">}}
  * Fix the output of the dipole. {{<MR "1576">}}
  * Fix underflows and fix the criterium for the x->0 limit erf function calculation. {{<MR "1577">}}
  * Fix a couple of issues related to an optional variable. {{<MR "1578">}}
  * Fix segfault with CUDA-aware MPI during finalization {{<MR "1588">}}
  * Fix a double precission issue in the mixings. {{<MR "1580">}}
  * Fix the output of the localized orbitals for the spinor case without spin-resolved orbitals. {{<MR "1621">}}
  * Fix an integer overflow in mesh_nearest_point {{<MR "1627">}}
  * Bugfix for oct-dielectric_function for lower space dimension than 3D. {{<MR "1626">}}
  * Fix problem with HGH and heavy elements {{<MR "1623">}}
  * Bugfix: LCAO and full-range hybrids {{<MR "1635">}}
  * Bugfix for Coulomb singularity for full range hybrids with k-points. {{<MR "1638">}}
  * Make curvilinear mesh work with domain parallelization {{<MR "1652">}}
  * Bugfix states_elec_calc_projections {{<MR "1657">}}
  * Bugfix for time propagation with MGGAs with ETRS and self-consistent ETRS propagators. {{<MR "1659">}}
  * Bugfix for MGGA with kinetic energy dependence, which was causing a jump a t=0 for TD calculations. {{<MR "1660">}}
  * The code was applying twice the gauge field kick if the delay was exactly commensurate with the time step. {{<MR "1634">}}
  * Bugfix: Gamma, k-point path and symmetries {{<MR "1669">}} (closes {{<issue "131">}})
  * Forbid to use k-point symmetries with SOC, as this is not supported. {{<MR "1671">}}
  * Fix a bug with the bsb pseudopotential filtering for llocal=-1. {{<MR "1670">}} (closes {{<issue "78">}})
  * Major improvement of the RMMDIIS algorithm by fixing some bugs and improving other aspects. {{<MR "1663">}}
  * Fix some problem with symmetries for the linear response TDTDM and improve the way to define the position of the hole. {{<MR "1674">}}
  * Fix a bug preventing to do spin-polarized bandstructure unfolding. {{<MR "1687">}} (closes {{<issue "551">}})
  * Fix the LCAO Alternative for complex wavefunctions with real orbitals. {{<MR "1696">}} (closes {{<issue "562">}})
  * Fix maxwell output  {{<MR "1688">}}
  * Several bugfixes with the Wannier90 utility for the w90_wannier mode. {{<MR "1690">}}, {{<MR "1708">}}
  * Coulomb integrals for DFT+U and DFT+U+V are now computed using FFTs by default. {{<MR "1683">}}
  * Bugfix for Sternheimer with k-point (and spin) parallelization. A test is added to cover this case and avoid that this case fails in the future. {{<MR "1711">}}
  * Fix a display issue for LCAOAlternative with k-point parallelization. {{<MR "1709">}}
  * Fix some temporary arrays caused by a missing contiguous flag. {{<MR "1723">}} (closes {{<issue "570">}})
  * Fix sizes of some arrays to avoid segfaults when copying to GPU. {{<MR "1700">}}
  * Bugfix: q0 and p0 was not properly read from inputfile for more than 1 photon mode {{<MR "1699">}}
  * Fix some bugs that occured when testing GPU runs on Ada. {{<MR "1762">}}
  * Fix a bug in the GPU calculation of the gradient and divergences in non-orthogonal cells. {{<MR "1774">}}
  * Fix a bug that can lead to segmentation faults in the MPI library. {{<MR "1769">}}
  * Fix restart issues. {{<MR "1768">}}, {{<MR "1779">}}, {{<MR "1783">}}
  * Various bugfixes: {{<MR "1637">}}, {{<MR "1639">}}, {{<MR "1641">}}, {{<MR "1644">}}, {{<MR "1650">}}, {{<MR "1654">}}, {{<MR "1673">}}, {{<MR "1685">}}, {{<MR "1686">}}, {{<MR "1689">}}, {{<MR "1703">}}, {{<MR "1705">}}, {{<MR "1719">}}, {{<MR "1720">}}, {{<MR "1721">}}, {{<MR "1722">}}, {{<MR "1747">}}, {{<MR "1758">}}

* Optimizations 
  * Optimization for the MGGA with kinetic-energy dependence. {{<MR "1471">}}
  * Use special memory for batch copy_to function only for CPU batches {{<MR "1506">}}
  * Adding OpenMP support for some DFT+U operations. {{<MR "1501">}}
  * Optimization of DFT+U in some cases. {{<MR "1502">}}, {{<MR "1622">}}
  * Improved OpenMP SIMD support. {{<MR "1477">}}
  * Faster application of the MGGA term (-)for vtau dependent MGGAs) for non-orthogonal cells. {{<MR "1493">}}
  * Batchify the divergence calculation for mgga {{<MR "1568">}}
  * Simplify the logic of the calculation of the Hartree theory level. {{<MR "1591">}}
  * Batchifying the Crank-Nicolson time propagator. {{<MR "1609">}}
  * Only switch to FFTW in case of clfft. {{<MR "1695">}} (closes {{<issue "559">}}) 
  * Add OpenMP support to some routines. {{<MR "1697">}}
  * Add OpenMP support for computing the 2D periodic ion-ion interaction. {{<MR "1701">}}
  * Optimization of MPI+GPU for periodic system with domain parallelization. {{<MR "1702">}}
  * Various optimizations: {{<MR "1581">}}, {{<MR "1584">}}, {{<MR "1712">}}, {{<MR "1715">}}, {{<MR "1771">}}


* Refactoring and Code cleanup
  * Refactoring {{<MR "1370">}}, {{<MR "1371">}}, {{<MR "1520">}}, {{<MR "1521">}}, {{<MR "1529">}}, {{<MR "1530">}}, {{<MR "1531">}}, {{<MR "1532">}}, {{<MR "1533">}} (closes {{<issue "520">}}), {{<MR "1534">}}, {{<MR "1536">}}, {{<MR "1537">}}, {{<MR "1541">}}, 
  {{<MR "1542">}}, {{<MR "1543">}}, {{<MR "1544">}}, {{<MR "1545">}}, {{<MR "1546">}},
  {{<MR "1547">}} (closes {{<issue "527">}}), {{<MR "1549">}}, {{<MR "1551">}}, {{<MR "1552">}}, {{<MR "1553">}}, {{<MR "1554">}}, {{<MR "1556">}}, {{<MR "1557">}}, {{<MR "1558">}}, {{<MR "1561">}}, {{<MR "1562">}},
  {{<MR "1570">}}, {{<MR "1571">}}, {{<MR "1592">}}, {{<MR "1593">}}, {{<MR "1594">}}, {{<MR "1595">}}, {{<MR "1596">}}, {{<MR "1597">}}, {{<MR "1612">}}, {{<MR "1613">}}, {{<MR "1616">}}, 
  {{<MR "1618">}}, {{<MR "1619">}}, {{<MR "1661">}}, {{<MR "1665">}}, {{<MR "1666">}}, {{<MR "1667">}}, {{<MR "1668">}}, {{<MR "1672">}}, {{<MR "1675">}}
  * Use norm2 where appropriate {{<MR "1569">}} (closes {{<issue "532">}})
  * Give more precise values to the coefficients of the spherical harmonics, to avoid radix conversion errors. {{<MR "1589">}}
  * Make MPI-2 mandatory. {{<MR "1563">}}
  * Other MPI related changes {{<MR "1620">}}, {{<MR "1624">}}, {{<MR "1633">}}, {{<MR "1640">}}
  * address compiler warnings  {{<MR "1586">}}
  * introduce variable kinds {{<MR "1629">}}, {{<MR "1631">}}, {{<MR "1632">}}
  * use large integers {{<MR "1642">}}, {{<MR "1648">}}, {{<MR "1649">}}, {{<MR "1652">}}

  * various code cleanup: {{<MR "1439">}}, {{<MR "1468">}}, {{<MR "1486">}} {{<MR "1511">}}, {{<MR "1564">}}, {{<MR "1566">}}, {{<MR "1565">}}, {{<MR "1572">}}, {{<MR "1579">}}, {{<MR "1607">}}, 
  {{<MR "1628">}}, {{<MR "1647">}}, {{<MR "1655">}}, {{<MR "1706">}}

* Documentation
  * updating documentation {{<MR "1454">}}, {{<MR "1523">}}, {{<MR "1694">}}, {{<MR "1713">}}, {{<MR "1718">}}
  * updating tutorials: {{<MR "1741">}}

* Testsuite: {{<MR "1575">}}, {{<MR "1573">}}, {{<MR "1590">}}, {{<MR "1599">}}, {{<MR "1600">}}, {{<MR "1601">}}, {{<MR "1725">}}, {{<MR "1734">}}, {{<MR "1753">}}

### Octopus 11

#### 11.4 - 2022-01-27

* Bug fixes:
  * Fix the MPI deadlock {{<MR "1617">}}
  * Fix a bug for CN propagator with moving ions. {{<MR "1615">}}
  * Fix output of the number of kpoints in many cases. {{<MR "1614">}}
  * Fix Libxc 5 detection. {{<MR "1611">}}
  * Bugfix: Correctly place the simd instruction. {{<MR "1610">}}
  * Fix some OpenMP clauses. {{<MR "1603">}}
  * Fix syntax of sleep command used in testsuite script that was unsupported in some flavors of *nix. {{<MR "1598">}}, closes {{<issue "536">}}
  * Fixing a missing k-point reduction in the perturbation. {{<MR "1560">}}

#### 11.3 - 2021-11-25 

* Bug fixes:
 * Fix a bug in the calculation of forces coming from the electric field, in case of multiple laser fields.  {{<MR "1517">}}
 * Bugfix: In case of restart inconsistency (not compatible k-points), this bugfix avoids a segmentation fault in case of state parallelization. {{<MR "1522">}}
 * Minor bugfix. {{<MR "1512">}} (closes {{<issue "515">}})
 * Fix an error message and remove an unnecessary double test. {{<MR "1519">}}
 * Fixes a problem that could occur with the ACBN0 functional when defining species like Ni1 and Ni2 to describe the same atoms with different magnetic moment. {{<MR "1516">}}
 * Fixes an assert for the gemm1 BLAS interface. {{<MR "1500">}} (closes {{<issue "508">}})

* Documentation:
 * Various updates and fixes. {{<MR "1495">}}, {{<MR "1498">}}, {{<MR "1510">}}, {{<MR "1548">}}


####  11.2 - 2021-10-18

* Bug fixes:
  * Fix meta-GGA test for libxc5. {{<MR "1456" >}} (closes {{<issue "463">}})
  * Fix the input for the oct-photoelectro_spetrum utility. {{<MR "1458">}}
  * Fix an order of destroy operations to avoid a warning. {{<MR "1464">}}
  * Fix output last TD iteration {{<MR "1465">}}
  * Fix of MGGA-OEP calculation. {{<MR "1467">}}
  * Fixes an overflow error occurring for some species. {{<MR "1473">}}
  * Fix a typo causing gfortran 11 to not compile the code. {{<MR "1476">}} (closes {{<issue "502">}})
  * Fix conversion to single precision implied by the real Fortran function. {{<MR "1481">}}
  * Revert indexing faulty indexing merge request. {{<MR "1487">}}
  * Bugfix for DFT+U for elements having a principal quantum number larger than 5. {{<MR "1494">}}
  * add GSL_RNG_SEED to the documentation. {{<MR "1485">}} (closes {{<issue "489">}})
  * Fix uninitialized variables. {{<MR "1472">}}
  * Various profiler bugfixes. {{<MR "1474">}}, {{<MR "1478">}}
  * Minor fixes. {{<MR "1466">}}, {{<MR "1480">}}, {{<MR "1490">}}

* Documentation:
  * Add tutorials. {{<MR "1457">}}, {{<MR "1459">}}
  * minor changes: {{<MR "1440">}}, {{<MR "1491">}}
####  11.1 - 2021-09-03

* Bug fixes:
  * Bugfix for Wannier90. {{< MR "1452" >}}
  * A fix for Loewdin orthogonalization (for DFT+U) for isolated systems. {{< MR "1448" >}}
  * Put back the output of the Casida excitations. {{< MR "1447" >}} (This was removed by mistake some time ago in {{< MR "1111" >}})
  * Fixes some calls to messages_not_implemented. {{< MR "1446" >}}
  * Fix Casida matrix calculation progress bar. {{< MR "1444" >}}
  * Fix Libxc functionals references output. {{< MR "1443" >}}
  * Fix integer overflow for large grids. {{< MR "1441" >}}

* Various changes:
  * Remove experimental status from several features. {{< MR "1451" >}}

####  11.0 - 2021-08-20

* New features and functionality:
  *  Implementation of Maxwell systems: {{< MR "784" >}}, {{< MR "989" >}}, {{< MR "1004" >}}, {{< MR "1034" >}}, {{< MR "1065" >}}, {{< MR "1423" >}}
  *  Multisystem support: {{< MR "956" >}}, {{< MR "1219" >}}, {{< MR "1223" >}}, {{< MR "1311" >}}
  *  Enable constant electromagnetic fields with ramp envelope functions for the Maxwell systems, and constant boundary conditions. {{< MR "922" >}} (closes {{<issue "342">}})
  *  Implementing two new Poisson solver for solving the Poisson equation on a submesh. {{< MR "923" >}} 
  *  Adding the possibility to specify static external potentials in the Hamiltonian. {{< MR "934" >}}
  *  Implements state-resolved multipole output. {{< MR "972" >}}
  *  Add tree output of profiling data. {{< MR "1007" >}}
  *  Improved PDOS output. {{< MR "1013" >}}, {{< MR "1140" >}}
  *  Add the possibility to compute the transient conductivity. {{< MR "1015" >}}
  *  Enable PFFT for periodic systems. {{< MR "1036" >}}
  *  First implementation of the ACE operator. {{< MR "1067" >}}
  *  Enable specification of photon modes from a file. {{< MR "1100" >}}
  *  QEDFT linear response implementation (Casida equation for electron-photon systems). {{< MR "1111" >}}
  *  Guess atomic occupations for improved LCAO calculations. {{< MR "1136" >}}
  *  Add frequency-dependent Sternheimer equation for electron-photon coupled systems. {{< MR "1137" >}}
  *  Add the possibility to subtract a reference magnetization for transient magnon spectroscopy. {{< MR "1144" >}}
  *  Enable DFTB Ehrenfest dynamics in dftbplus system. {{< MR "1161" >}}
  *  Implementation of the force-balance-equation exchange functional. {{< MR "1166" >}}
  *  Add the support of the spin-polarized case for the oct-wannier90 utility. {{< MR "1265" >}}
  *  Adding an option to output the exchange-correlation torque. {{< MR "1296" >}}
  *  Implemented the restart function with FreezeHXC option. {{< MR "1308" >}}
  *  The Slater now supports k-points. {{< MR "1310" >}}
  *  Change Output, TDOutput and OutputHow to allow more options. {{< MR "1312" >}}
  *  Introducing a new run method, generalized Kohn-Sham, for running hybrid functionals and MGGAs. The MGGA can now be treated using the OEP framework. {{< MR "1322" >}}
  *  Add the Maxwell-Kohn-Sham time-propagation method formulated within the framework of QEDFT relevant for strongly coupled light-matter systems. {{< MR "1372" >}} (closes {{<issue "453">}})
  *  Add the possibility to compute the +U (and +V) using a species full delta. {{< MR "1384" >}}

* Various changes
  *  Runs without using the GPU will now by default fail on GPU machines. {{< MR "873" >}} (closes {{<issue "251">}})
  *  Add the possibility to disable the CGAL library. {{< MR "1068" >}}
  *  Removing the force convergence criteria. {{< MR "1081" >}}
  *  Remove support for Libxc 3. {{< MR "1164" >}}
  *  Add support for upcoming Libxc 5.1 and remove support for Libxc 5.0. {{< MR "1165" >}}
  *  Improve Broyden mixing. Implement restarting of mixing. {{< MR "1167" >}}
  *  Rewrite the HGH pseudopotential interface, to be able to parse files in the Abinit format. {{< MR "1279" >}}
  *  Reimplementing KLI for spinors, especially including the explicit solution of the equation. {{< MR "1317" >}}
  *  Check for kxc & fxc support in libxc >= 5.1 {{< MR "1418" >}}
  *  Adding more internal checks and debugging output: {{< MR "912" >}}, {{< MR "1028" >}}, {{< MR "1142" >}}, {{< MR "1171" >}}, {{< MR "1262" >}}, {{< MR "1355" >}},  {{< MR "1373" >}}, {{< MR "1420" >}} (closes {{<issue "473">}})
  *  Code cleanup: {{< MR "328" >}}, {{< MR "927" >}}, {{< MR "1127" >}}, {{< MR "1139" >}}, {{< MR "1146" >}}, {{< MR "1147" >}}, {{< MR "1054" >}}, {{< MR "1156" >}}, {{< MR "1177" >}}, {{< MR "1191" >}}, {{< MR "1196" >}}, {{< MR "1209" >}}, {{< MR "1368" >}}, {{< MR "1424" >}}
  *  Minor improvements {{< MR "917" >}}, {{< MR "933" >}}, {{< MR "945" >}}, {{< MR "975" >}}, {{< MR "1005" >}}, {{< MR "1022" >}}, {{< MR "1023" >}}, {{< MR "1237" >}}, {{< MR "1248" >}}, {{< MR "1305" >}}

* Refactoring:
  *  Multisystem support: {{< MR "895" >}}, {{< MR "896" >}}, {{< MR "897" >}}, {{< MR "932" >}}, {{< MR "940" >}}, {{< MR "941" >}}, {{< MR "953" >}}, {{< MR "961" >}}, {{< MR "962" >}}, {{< MR "971" >}}, {{< MR "974" >}}, {{< MR "979" >}}, 
    {{< MR "981" >}}, {{< MR "982" >}}, {{< MR "994" >}}, {{< MR "1001" >}}, {{< MR "1002" >}},{{< MR "1010" >}}, {{< MR "1016" >}}, {{< MR "1019" >}}, {{< MR "1113" >}}, {{< MR "1116" >}}, {{< MR "1221" >}}, {{< MR "1222" >}}, {{< MR "1244" >}}, 
    {{< MR "1254" >}}, {{< MR "1278" >}}, {{< MR "1283" >}}, {{< MR "1286" >}}
  *  Move to object oriented design: 
{{< MR "938" >}}, {{< MR "976" >}}, {{< MR "1026" >}}, {{< MR "1105" >}}, {{< MR "1173" >}}, {{< MR "1321" >}}, {{< MR "1331" >}}, {{< MR "1335" >}}, {{< MR "1342" >}}, {{< MR "1350" >}}, {{< MR "1352" >}}, {{< MR "1359" >}}, {{< MR "1360" >}}
  *  Maxwell systems: {{< MR "1038" >}}, {{< MR "1048" >}}, {{< MR "1049" >}}, {{< MR "1337" >}}
  *  Simulation boxes and geometry: {{< MR "1169" >}}, {{< MR "1178" >}}, {{< MR "1197" >}}, {{< MR "1200" >}}, {{< MR "1201" >}}, {{< MR "1218" >}}, {{< MR "1225" >}}, {{< MR "1226" >}}, {{< MR "1227" >}}, {{< MR "1235" >}}, {{< MR "1243" >}}, {{< MR "1249" >}}, {{< MR "1251" >}}, {{< MR "1255" >}}, {{< MR "1257" >}}, {{< MR "1272" >}}, {{< MR "1284" >}}, {{< MR "1285" >}}, {{< MR "1297" >}}, {{< MR "1300" >}}, {{< MR "1315" >}}, {{< MR "1318" >}}, {{< MR "1323" >}}, {{< MR "1327" >}}, 
    {{< MR "1341" >}}, {{< MR "1357" >}}
  *  Grids and meshes: {{< MR "1134" >}}, {{< MR "1135" >}}, {{< MR "1138" >}}, {{< MR "1230" >}}, {{< MR "1238" >}}, {{< MR "1247" >}}, {{< MR "1260" >}}, {{< MR "1263" >}}, {{< MR "1295" >}}, {{< MR "1307" >}}, {{< MR "1343" >}}, {{< MR "1349" >}}
  *  Eigensolver: {{< MR "1014" >}}, {{< MR "1280" >}}
  *  Low level code: {{< MR "1021" >}}, {{< MR "1084" >}}, {{< MR "1175" >}}, {{< MR "1183" >}}, {{< MR "1184" >}}, {{< MR "1185" >}}, {{< MR "1186" >}}, {{< MR "1189" >}}, {{< MR "1193" >}}, {{< MR "1267" >}}
  *  Various: {{< MR "939" >}},{{< MR "946" >}}, {{< MR "950" >}}, {{< MR "952" >}}, {{< MR "959" >}}, {{< MR "969" >}}, {{< MR "1020" >}}, {{< MR "1027" >}}, {{< MR "1040" >}}, {{< MR "1055" >}}, {{< MR "1056" >}}, {{< MR "1070" >}}, {{< MR "1078" >}}, {{< MR "1087" >}}, {{< MR "1099" >}}, {{< MR "1110" >}}, {{< MR "1126" >}}, {{< MR "1145" >}}, {{< MR "1148" >}}, {{< MR "1150" >}}, {{< MR "1151" >}}, {{< MR "1152" >}}, {{< MR "1157" >}}, {{< MR "1160" >}}, {{< MR "1163" >}}, {{< MR "1174" >}}, {{< MR "1181" >}}, 
{{< MR "1188" >}}, {{< MR "1192" >}}, {{< MR "1198" >}}, {{< MR "1199" >}}, {{< MR "1207" >}}, {{< MR "1208" >}}, {{< MR "1210" >}}, {{< MR "1212" >}}, {{< MR "1213" >}}, {{< MR "1215" >}}, {{< MR "1216" >}}, {{< MR "1217" >}}, {{< MR "1220" >}}, {{< MR "1224" >}}, {{< MR "1228" >}}, {{< MR "1232" >}}, {{< MR "1233" >}}, {{< MR "1234" >}}, {{< MR "1236" >}}, {{< MR "1242" >}}, {{< MR "1252" >}}, {{< MR "1253" >}}, {{< MR "1256" >}}, {{< MR "1269" >}}, {{< MR "1292" >}}, {{< MR "1294" >}}, {{< MR "1298" >}}, {{< MR "1301" >}}, 
{{< MR "1313" >}}, {{< MR "1316" >}}, {{< MR "1334" >}}, {{< MR "1340" >}}, {{< MR "1344" >}}, {{< MR "1347" >}}, {{< MR "1354" >}}, {{< MR "1367" >}}, {{< MR "1369" >}}

* Bug fixes:
  *  Bugfixes relating to the eigensolver. {{< MR "1204" >}}, {{< MR "1250" >}}, {{< MR "1281" >}}
  *  Bugfix electron-photon for Sternheimer: {{< MR "1425" >}}, {{< MR "1339" >}}
  *  Bugfix for LCAO. {{< MR "1129" >}}, {{< MR "1205" >}}, {{< MR "1375" >}}, {{< MR "1392" >}}, {{< MR "1382" >}}
  *  Bugfixes for hybrid functionals, and OEP. {{< MR "1306" >}}, {{< MR "1314" >}}, {{< MR "1376" >}}, {{< MR "1408" >}}
  *  Fix several Maxwell issues {{< MR "926" >}}, {{< MR "1024" >}} (closes -391), {{< MR "1011" >}}, {{< MR "1120" >}}, {{< MR "1426" >}}
  *  Fix some MPI related bugs: {{< MR "1008" >}}, {{< MR "1158" >}}, {{< MR "1170" >}}, {{< MR "1245" >}}, {{< MR "1379" >}}
  *  Fix the k-resolved output for spinors. {{< MR "1012" >}}
  *  More fixes to the clocks and to the interactions update. {{< MR "1017" >}}
  *  Forbidding the combination curvilinear plus mask for the integral, as this is not supported. {{< MR "1044" >}}
  *  Fix division by zero in classical Coulomb interaction. {{< MR "1104" >}}
  *  Fixes of the conductivity utility for 1D calculations. {{< MR "1122" >}}
  *  Fixes, assertions and forbidden options when calculating periodic forces. {{< MR "1304" >}}
  *  Bugfix: Forbid to use GuessMagnetDensity with Hartree-Fock calculations, as this has no meaning. {{< MR "1309" >}}
  *  Fixed a bug for the output of eivenvalues in the time-dependent run. {{< MR "1319" >}}
  *  Bugfix for 3D Ewald summation. {{< MR "1324" >}} 
  *  Fix the read-in problem of the photon modes from an external file. {{< MR "1338" >}} (closes {{<issue "450">}})
  *  Fix Born-Oppenheimer dynamics with "AbsorbingBoundaries=mask". {{< MR "1346" >}} (closes {{<issue "454">}})
  *  Bugfix for the multigrid interpolation. {{< MR "1348" >}}
  *  Fix to prefilter preconditioner for periodic systems. {{< MR "1361" >}}
  *  Bugfixes for MGGAs with vtau. {{< MR "1362" >}}
  *  Fix problem when Born charges output. {{< MR "1365" >}}
  *  Fix the calculation of the off-diagonal term of the current matrix. {{< MR "1366" >}}
  *  Bugfix: HGH with semicore for elements 71 to 78. {{< MR "1378" >}}
  *  Fix intent in interface to cuda_init. {{< MR "1383" >}}
  *  Fix support for build with the Intel compilers {{< MR "1389" >}}
  *  Deprecate an old name for triggering the use of a Scalapack compatible layout which has changed in the mean time. {{< MR "1390" >}}
  *  Bugfix for the photoemission in periodic systems. {{< MR "1393" >}}
  *  Bugfix: Select between the two possible options of reading photon modes. {{< MR "1402" >}}
  *  Bugfix: Remove a push/pop that prevents running with Debug=trace_file. {{< MR "1404" >}}
  *  Bugfix: Wrong application of phases to ghostpoints. {{< MR "1405" >}}
  *  Fix the segfaults with the recent GNU compiler. {{< MR "1409" >}}
  *  Bugfix: Do not initialize a restart for dumping for 0 iterations. {{< MR "1419" >}} 
  *  Bugfixes related to the documentation. {{< MR "1422" >}} (closes {{<issue "284">}})
  *  Bugfixes related to undeclared variables. {{< MR "1387" >}}
  *  Bugfixes related to memory issues. {{< MR "1417" >}}, {{< MR "1406" >}}, {{< MR "1401" >}}, {{< MR "1095" >}}, {{< MR "1329" >}}, {{< MR "1303" >}}, {{< MR "1090" >}}
  *  Fixes related to warnings: {{< MR "1117" >}}, {{< MR "1088" >}}, {{< MR "1098" >}}, {{< MR "1094" >}}
  *  minor bugfixes: {{< MR "899" >}}, {{< MR "930" >}}, {{< MR "944" >}}, {{< MR "955" >}}, {{< MR "965" >}}, {{< MR "966" >}}, {{< MR "1009" >}}, {{< MR "1030" >}}, {{< MR "1041" >}}, {{< MR "1047" >}}, {{< MR "1101" >}}, {{< MR "1115" >}}, {{< MR "1143" >}}, {{< MR "1182" >}}, {{< MR "1187" >}}, {{< MR "1194" >}}, {{< MR "1203" >}}, {{< MR "1239" >}}, {{< MR "1246" >}}, {{< MR "1258" >}}, {{< MR "1259" >}}, {{< MR "1261" >}}, {{< MR "1264" >}}, {{< MR "1270" >}}, {{< MR "1271" >}}, {{< MR "1273" >}}, {{< MR "1277" >}},
{{< MR "1282" >}}, {{< MR "1288" >}}, {{< MR "1289" >}}, {{< MR "1291" >}}, {{< MR "1332" >}} 

* Performance and Optimizations:
  *  Make use of scalapack or elpa possible for Casida. {{< MR "1106" >}}
  *  Minor optimization for MGGA with Laplacian dependence and with domain parallelization. {{< MR "1364" >}}
  *  Improved stability for Pulay and DIIS mixings. {{< MR "1407" >}}
  *  updated the test file and optimized Sternheimer {{< MR "1427" >}}
  *  Optimizations to LCAO: {{< MR "1229" >}}, {{< MR "1240" >}}, {{< MR "1241" >}}
  *  Optimizations of the Eigensolver: {{< MR "1075" >}}, {{< MR "1102" >}}, {{< MR "1153" >}}, {{< MR "1179" >}}, {{< MR "1190" >}}, {{< MR "1195" >}}, {{< MR "1320" >}}
  *  Optimizations of the Maxwell code: {{< MR "1037" >}}, {{< MR "1042" >}}, {{< MR "1043" >}}, {{< MR "1045" >}}, {{< MR "1051" >}}
  *  Minor optimizations.  {{< MR "967" >}}, {{< MR "984" >}}, {{< MR "1006" >}}, {{< MR "1018" >}}, {{< MR "1052" >}}, {{< MR "1058" >}}, {{< MR "1121" >}}, {{< MR "1206" >}}, {{< MR "1214" >}}, {{< MR "1266" >}}, {{< MR "1276" >}}, {{< MR "1325" >}}

* Testsuite:
  *  This update to the testsuite allows to test for failing runs. {{< MR "614" >}} (closes {{<issue "153">}})
  *  Update references and tolerances. {{< MR "985" >}}, {{< MR "1414" >}}, {{< MR "1415" >}}. {{< MR "14165" >}}, {{< MR "1410" >}}, {{< MR "1411" >}}, {{< MR "1412" >}}, {{< MR "1413" >}}, {{< MR "1399" >}}, {{< MR "1398" >}}, {{< MR "1400" >}}, {{< MR "1396" >}}, {{< MR "1397" >}}, {{< MR "1394" >}}, {{< MR "1386" >}}, {{< MR "1062" >}}, {{< MR "1066" >}}, {{< MR "1109" >}}, {{< MR "998" >}}, {{< MR "949" >}}, {{< MR "931" >}}
  *  Add new tests. {{< MR "1032" >}}, {{< MR "1333" >}}, {{< MR "1119" >}}, {{< MR "1097" >}}, {{< MR "963" >}}
  *  Implement a unit test for the linear solvers. At the moment, only CG and QMR are tested. {{< MR "1033" >}}
  *  Remove the use of the lcao_full option from most of the testsuite. {{< MR "1124" >}}
  *  Extend and improve local multipoles tests. {{< MR "1132" >}}
  *  Various. {{< MR "1046" >}}, {{< MR "1395" >}}, {{< MR "1202" >}}, {{< MR "1074" >}}, {{< MR "1108" >}}, {{< MR "1290" >}}, {{< MR "1130" >}}, {{< MR "1125" >}}, {{< MR "1118" >}}, {{< MR "935" >}}, {{< MR "951" >}}

* Buildsystem:
  *  Fix autoconf warnings. {{< MR "1083" >}}
  *  Use shell variable instead of automake variable. {{< MR "1287" >}}
  *  Add support for dftb+. {{< MR "783" >}}
  *  Improve configure autodetection. {{< MR "1086" >}}
  *  This MR fixes the compilation of the code for gfortran 10 when no extra FCFLAGS are given. {{< MR "1092" >}}
  *  m4 macro for CGAL. {{< MR "1093" >}}, {{< MR "1080" >}}
  *  Build system: use a shared library to reduce total size of executables. {{< MR "1003" >}}
  *  Adding a check for the error code of Metis. {{< MR "1039" >}} (closes {{<issue "412">}})

* Documentation:
  *  Minor fix to the documentation of some modelmb variables. {{< MR "1302" >}}
  *  Moved documentation and tutorial files into the source repository {{< MR "1433" >}} 


### Octopus 10

####  10.5 - 2021-05-31  

* Bugfixes:
  * Update the pseudo-dojo LDA pseudo tables to version 0.4.1. {{< MR "1231" >}}
  * Add the missing k-point contribution to the off-diagonal term of the kinetic energy density. {{< MR "1268" >}}
  * Fix the unit of the data read by the oct-conductivity utility in case of ev_angstrom TD calculation. {{< MR "1275" >}}
  * Bugfix for spin polarized OEP full calculation. {{< MR "1299" >}}
  * Workaround for a bug in intel 2016.1.150. {{< MR "1330" >}}

####  10.4 - 2021-02-04  

* Bugfixes:
  * Fix a wrong copy of to the old vxc potential for BB mixing of OEP. {{< MR "1172" >}}
  * Add the missing support of f electrons in the ylmr routine. {{< MR "1168" >}}
  * Bugfix for CG with complex wavefunctions. {{< MR "1128" >}}
  * Fix several issues flagged by Valgrind. {{< MR "1114" >}}
  * Fix several minor unit conversion problems with convergence criteria. {{< MR "1112" >}}
  * Fix incorrect phase initialization when running gamma-only calculations {{< MR "1103" >}}
  * Fix problems related to divisions by zero. {{< MR "1091" >}}
  * Bugfix for oct-wannier90. {{< MR "1096" >}}

####  10.3 - 2020-11-08  

* Bugfixes:
  * Fix the preconditioner_apply_batch call for multigrid preconditioner in combination of RMMDIIS. {{< MR "1082" >}}
  * Fix uninitialized variables. {{< MR "1079" >}}
  * Fix contiguous statements for newer gfortran versions (fixes gcc-9 issues). {{< MR "1063" >}}
  * Minor bugfix for DFT+U with domain parallelization. {{< MR "1073" >}}
  * Fix an array bound mismatch. The range of the DOS is now independent of the k-points in the path. {{< MR "1071" >}}
  * Fix the test for non-orthogonality, that was not behaving properly. {{< MR "1064" >}}
  * Bugfix in the non-batchified CG linear solver. {{< MR "1061" >}}
  * Minor fixes to the test system. {{< MR "1076" >}} {{< MR "1072" >}} {{< MR "1064" >}}

####  10.2 - 2020-10-28  

* Bugfixes:
  * Forbid to run the code for unsupported cases. {{< MR "915" >}}
  * Fix the use of the move_ions argument for time propagators. {{< MR "942" >}}
  * Fix units of CUBE file format. {{< MR "1035" >}}

####  10.1 - 2020-08-17  

* Bugfixes:
  * Fix some problems with the Slater potential and improve the numerical stability for the case of singular matrices. {{< MR "943" >}}
  * Fix some uninitialized variables in the code. {{< MR "983" >}} {{< MR "992" >}} {{< MR "993" >}}
  * Fix an issue which prevented the code from running on the rtx2080ti machines. {{< MR "990" >}}
  * Fix preconditioners for non-orthogonal cells. {{< MR "861" >}}

####  10.0 - 2020-07-20  

* New features and functionality:
  * Add the possibility to output the dipole matrix elements. {{< MR "346" >}}
  * Add the missing term in the total energy coming from static electric field. {{< MR "447" >}}
  * Add the possibility of computing the two-body integrals with k-points. {{< MR "460" >}}
  * Add option to use conjugate gradients for the orbital optimization in the rdmft routine. {{< MR "462" >}}
  * Add the dressed RDMFT construction, which allows for treating electronic systems that interact with a cavity photon mode. {{< MR "472" >}}
  * Add a utility to interface Octopus with Wannier90. {{< MR "497" >}}
  * Add the variable {{< variable "TDFreezeOrbitals" >}} with MGGA functionals and restarting. {{< MR "519" >}}
  * Add a new utility to perform band-structure unfolding. {{< MR "525" >}}
  * Add a k-point parallelization support for {{<code-inline>}}{{< variable "TDOutput" >}} =td_proj{{</code-inline>}} and forbidding {{< code-inline >}}{{< variable "TDOutput" >}}=populations{{</code-inline>}} for solids, as it is not supported. {{< MR "526" >}}
  * Enable GPU acceleration for solids. {{< MR "529" >}}
  * Add support for {{<code-inline>}}{{<variable "TDOutput">}} = td_occup{{</code-inline>}} to run in parallel. {{< MR "537" >}}
  * Add a variable {{<variable "PropagationSpectrumMinEnergy">}} to control the minimum energy in Fourier transforms. {{< MR "545" >}}
  * Add a timer for restart. {{< MR "556" >}}, {{< MR "573" >}}
  * Started adding support for Maxwell systems (currently experimental, as unfinished). {{< MR "618" >}}, {{< MR "623" >}}, {{< MR "684" >}}, {{< MR "710" >}}
  * Started adding support for multisystems (currently experimental, as unfinished). {{< MR "613" >}}, {{< MR "643" >}}, {{< MR "664" >}}, {{< MR "777" >}}, {{< MR "789" >}}, {{< MR "791" >}}, {{< MR "794" >}}, {{< MR "796" >}}, {{< MR "876" >}}, {{< MR "881" >}}, {{< MR "818" >}}, {{< MR "801" >}}, {{< MR "810" >}}, {{< MR "812" >}}, {{< MR "907" >}}
  * OEP: fix spin polarized mode and add new mixing scheme. {{< MR "558" >}}
  * The code now writes the bandgap for solids in the static/info file. {{< MR "587" >}}
  * Implementation of the magnon kick for periodic systems. {{< MR "606" >}} {{< MR "905" >}}
  * Extension of the DFT+U to include the intersite V. {{< MR "617" >}}
  * Enable domain parallelization for intersite interaction (DFT+U+V). {{< MR "658" >}}
  * Add support for NVTX to aid GPU profiling. {{< MR "660" >}}
  * Implementation of the one-photon OEP functional of QEDFT described in Phys. Rev. Lett. 115, 093001 (2015) and ACS Photonics 5, 3, 992-1005 (2018). {{< MR "662" >}}
  * Add support for preconditioning when using the CG minimizer of RDMFT. {{< MR "672" >}}
  * Add support for CUDA-aware MPI. {{< MR "675" >}}
  * Allow for restarting a DFT+U+V calculation. {{< MR "683" >}}
  * Add the possibility to compute a bandstructure from the time-evolved states. {{< MR "690" >}}
  * Add the possibility to profile the number of call to file open and close. {{< MR "708" >}}
  * Add yaml output for the internal profiling of timings. {{< MR "717" >}}
  * Add a mask for which periodic boundaries are replaced by zero boundary conditions. {{< MR "726" >}}
  * Add output of forces in exponential format in static/info. {{< MR "729" >}}
  * Add the possibility to use the ISF Poisson solver to compute the Coulomb integrals for the DFT+U. {{< MR "752" >}}
  * Add support for complex-to-complex FFT in the Poisson solver. {{< MR "749" >}}
  * Implementation of the Coulomb singularity for the Coulomb integrals in solids. {{< MR "757" >}}
  * Add support for running Hartree-Fock and hybrid calculations for solids. {{< MR "773" >}}
  * Add performance regression testsuite. {{< MR "781" >}}
  * Add support for the CAM omega parameter for the FFT Poisson solver. {{< MR "786" >}}
  * Add support for gpg signatures in git commits. {{< MR "788" >}}
  * Add support for spin-resolved photon-KLI. {{< MR "805" >}}
  * Add support for upcoming Libxc 5, remove support for Libxc 2 and mark 3 as deprecated. {{< MR "808" >}}, {{< MR "809" >}}, {{< MR "811" >}}
  * Add support for unary plus in input files. {{< MR "847" >}}
  * A new, faster, implementation of the Slater approximation to the OEP equations, for both collinear and non-collinear spins. {{< MR "900" >}}
  * Adding profilers to the different force contributions. {{< MR "914" >}}

* Various changes:
  * Removed the warning from QMR about convergence. {{< MR "552" >}}
  * Make threshold in energy change during CG iterations a parameter. {{< MR "557" >}}
  * Make ps_debug print the density for the long range potential. {{< MR "566" >}}
  * Stop code if factor for filter preconditioner out of range. {{< MR "571" >}}
  * Forbid unsupported combination of DFT+U with {{<variable "TDFreezeOrbitals">}}. {{< MR "576" >}}
  * When a segment with zero point is specified in the k-point path, the length between the two points is not taken into account. {{< MR "586" >}}
  * Change kpoints error message to warning in some cases. {{< MR "721" >}}
  * Remove the single precision related parts of the code. {{< MR "731" >}}
  * Small change in the behavior of the variable {{<variable "SymmetriesCompute">}} for periodic systems. {{< MR "748" >}}
  * The code now stores and prints the cell angles in periodic systems. {{< MR "753" >}}
  * This branch enhances clock printing and setting. {{< MR "792" >}}
  * The range of the soft Coulomb potential is now set by according to the {{<variable "SpeciesProjectorSphereThreshold" >}} variable. {{< MR "797" >}}
  * Improve the documentation of the {{<variable "DOSComputePDOS">}} variable. {{< MR "823" >}}
  * Change stopping criteria for the SCF cycle: tighter convergence thresholds; have to be fulfilled twice in a row. {{< MR "834" >}}
  * Marking geometry optimization for solids as experimental. {{< MR "836" >}}
  * Removing single precision code that is not used anymore. {{< MR "839" >}}
  * Always write the full {{< file "td.general/laser" >}} file to take into account possible changes in the total propagation time. {{< MR "844" >}}
  * Simplified logic in the gauge-field code. {{< MR "867" >}}
  * Fix the behavior of lalg_determinant and lalg_inverter to be more meaningful. {{< MR "884" >}} 
  * Improving the documentation of the variable {{<variable "AOThresold">}}. {{< MR "911" >}}
  * Merge the input options related to the photon modes for RDMFT and OEP. {{< MR "925" >}}

* Refactoring: 
  * Create abstract classes.  {{< MR "605" >}}, {{< MR "640" >}}, {{< MR "611" >}}, {{< MR "642" >}}, {{< MR "645" >}}
  * Move existing structures to classes. {{< MR "581" >}}, {{< MR "585" >}}, {{< MR "595" >}}, {{< MR "588" >}}, {{< MR "603" >}}, {{< MR "607" >}}, {{< MR "609" >}}, {{< MR "624" >}}, {{< MR "644" >}}, {{< MR "716" >}}, {{< MR "734" >}}, {{< MR "894" >}}
  * Other code-refactoring. {{< MR "577" >}}, {{< MR "636" >}}, {{< MR "638" >}}, {{< MR "648" >}}, {{< MR "649" >}}, {{< MR "651" >}}, {{< MR "668" >}}, {{< MR "676" >}}, {{< MR "698" >}}, {{< MR "736" >}}, {{< MR "744" >}}, {{< MR "763" >}}, {{< MR "764" >}}, {{< MR "768" >}}, {{< MR "769" >}}, {{< MR "782" >}}, {{< MR "816" >}}, {{< MR "842" >}}, {{< MR "893" >}}, {{< MR "906" >}}, {{< MR "919" >}}

* Bugfixes:
  * Bugfix for the Laplacian in the case of non-orthogonal cells. {{< MR "484" >}}, {{< MR "521" >}}
  * Fix a bug preventing to run DFT+U if a species was not a pseudopotential. {{< MR "541" >}}
  * Bugfix: the value of overlap was ignored by submesh_broadcast. {{< MR "543" >}}, {{< MR "544" >}}
  * Fix a bug that was leading to zeros in the diff for states using rmmdiis. {{< MR "569" >}}
  * Fix various problems related to the header of td_occup output for isolated systems in states parallelization. {{< MR "629" >}}
  * Bugfix: The states parallelization for the projections was not working in case of multiple parallelization scheme. {{< MR "661" >}}
  * Fix some internal inconsistencies between scf and {{<code unocc>}}calculations. {{< MR "679" >}}
  * Bugfix: When restarting a DFT+U calculation, the code now recomputes the DFT+U energy at the same time as the potential. {{< MR "681" >}}
  * Bugfix: for the Flux module (aka tsurff). {{< MR "682" >}}, {{< MR "691" >}}
  * Bugfix: Fix 1D Coulomb with screening. {{< MR "685" >}}
  * Fix incorrect syntax in documentation of oct-unfold that lead to an incorrect section for several variables. {{< MR "699" >}}
  * Bugfix: Add check for periodic boundary conditions. {{< MR "706" >}}
  * Improved support of hybrid functionals. Add support for CAM and MVORB functionals. {{< MR "707" >}}
  * Fix a problem with the input variable {{<variable "FFTPreparePlan">}}, which was not working. {{< MR "779" >}}
  * Bugfix: Fix a problem with angles for low dimensions. {{< MR "785" >}}
  * Forbid running a combination of options which are not compatible. {{< MR "822" >}}
  * Bugfix: In case the code does not find the molecule spacegroup, we do not try to get the name of the spacegroup. {{< MR "824" >}}
  * Disable Casida for some combinations. {{< MR "828" >}}
  * Remove potential deadlock in PNFFT (Closes -187 (closed)) {{< MR "829" >}}
  * Update atomic masses in {{<file "share/pseudopotentials/elements">}}. {{< MR "831" >}}
  * Adding a test to prevent computing the bandgap when there are no extra states. {{< MR "843" >}}
  * Bugfix: Resolve "{{<code-inline>}}{{<variable "EigenSolver">}} = plan{{</code-inline>}} is broken and gives a segmentation fault" {{< MR "845" >}}
  * Bugfix: This MR fixes a bug in the states_elec_generate_random function. {{< MR "848">}}
  * Improve the Lapack interface and fix several problems and potential bugs. {{< MR "850" >}}, {{< MR "856" >}}
  * Fixes to the RMDIIS eigensolver. {{< MR "851" >}}
  * Fixes a bug in {{< file "states_elec_generate_random" >}}, which prevented consistent random wave functions between serial and parallel runs. {{< MR "857" >}}
  * Bugfix: the lcao was not correct for periodic system with Gamma only calculations. {{< MR "865" >}}
  * Bugfix: Fixed bug in the calculation of spin-polarized hhg spectrum from multipoles. {{< MR "869" >}}
  * Improve error message for missing pseudopotential {{< MR "872" >}}
  * Bugfix: In some case the code was not using properly r_small, which is a protected value, and should not be zero. {{< MR "874" >}}
  * Bugfix: If requested, the density is symmetrized now also for the LCAO part. {{< MR "875" >}}
  * Bugfix: The evolution eigensolver is not compatible with smearing of the occupations. {{< MR "908" >}}
  * Bugfix: Change the default to {{<code "lcao_states">}}, as {{<code "lcao_full">}} is not working properly. {{< MR "909" >}}
  * Bugfixes for the calculation of the paramagnetic current and KED for spinors and k-points. {{< MR "920" >}}
  * Bugfix: The OEP routines should not be called if the slater potential is requested. Otherwise, both are computed. {{< MR "928" >}}
  * Bugfix: Fix the output of potentials for the spinor case. {{< MR "929" >}}
  * Bugfix for LCAO states and Fermi energy calculation in case of degenerated states at the Fermi energy. {{< MR "937" >}}
  * Bugfix: fixes the off-diagonal terms of some noncollinear quantities. {{< MR "954" >}}
  * Bugfix: Fix a problem preventing running MGGA with energy functionals. {{< MR "957" >}}
  * Bugfix: Fixes the {{< file "lcao_init" >}} routine for spinors. {{< MR "958" >}}
  * Bugfixes related to memory leaks. {{< MR "814" >}}, {{< MR "888" >}}
  * Bugfixes related to GPU code. {{< MR "542" >}}, {{< MR "548" >}}, {{< MR "659" >}}, {{< MR "701" >}} {{< MR "738" >}}
  * Bugfixes: uninitialized variables. {{< MR "596" >}}, {{< MR "692" >}}, {{< MR "712" >}}, {{< MR "813" >}}
  * Bugfixes related to compiler warnings. {{< MR "751" >}}, {{< MR "852" >}}
  * Other bugfixes: {{< MR "513" >}}, {{< MR "514" >}}, {{< MR "546" >}}, {{< MR "547" >}}, {{< MR "562" >}}, {{< MR "616" >}}, {{< MR "639" >}}, {{< MR "647" >}}, {{< MR "654" >}}, {{< MR "697" >}}, {{< MR "702" >}}, {{< MR "713" >}}, {{< MR "718" >}}, {{< MR "723" >}}, {{< MR "725" >}}, {{< MR "730" >}}, {{< MR "762" >}}, {{< MR "770" >}}, {{< MR "798" >}}, {{< MR "815" >}}, {{< MR "819" >}}, {{< MR "825" >}}, {{< MR "826" >}}, {{< MR "827" >}}, {{< MR "832" >}}, {{< MR "833" >}}, {{< MR "837" >}}, {{< MR "853" >}}, {{< MR "855" >}}, {{< MR "862" >}}, {{< MR "864" >}}, {{< MR "870" >}}, {{< MR "885" >}}, {{< MR "890" >}}, {{< MR "892" >}}, {{< MR "903" >}}, {{< MR "904" >}}, {{< MR "910" >}} 

* Performance and Optimizations:
  * Various simplifications and optimization regarding the use of batches. {{< MR "492" >}}, {{< MR "494" >}}, {{< MR "498" >}}, {{< MR "510" >}}, {{< MR "511" >}}, {{< MR "538" >}}, {{< MR "554" >}}, {{< MR "765" >}}, {{< MR "766" >}}, {{< MR "767" >}}
  * Remove unnecessary operations. {{< MR "522" >}}, {{< MR "561" >}}, {{< MR "563" >}}, {{< MR "564" >}}, {{< MR "575" >}}, {{< MR "599" >}}, {{< MR "619" >}}, {{< MR "680" >}}, {{< MR "871" >}}, {{< MR "901" >}}
  * Convert to 'batch' framework. {{< MR "587" >}}, {{< MR "650" >}}, {{< MR "652" >}}, {{< MR "653" >}}, {{< MR "663" >}}, {{< MR "667" >}}, {{< MR "688" >}}, {{< MR "700" >}}, {{< MR "743" >}}, {{< MR "758" >}}, {{< MR "759" >}}, {{< MR "887" >}}
  * Optimizations for the GPU code. {{< MR "527" >}}, {{< MR "542" >}}, {{< MR "549" >}}, {{< MR "804" >}}, {{< MR "860" >}}, {{< MR "669" >}}, {{< MR "671" >}}, {{< MR "674" >}}, {{< MR "704" >}}, {{< MR "732" >}}, {{< MR "771" >}}, {{< MR "806" >}}
  * Improved parallelism. {{< MR "633" >}}, {{< MR "657" >}}, {{< MR "673" >}}, {{< MR "686" >}}, {{< MR "709" >}}, {{< MR "733" >}}, {{< MR "882" >}}
  * Other improvements and optimizations: {{< MR "528" >}}, {{< MR "559" >}}, {{< MR "578" >}}, {{< MR "579" >}}, {{< MR "593" >}}, {{< MR "602" >}}, {{< MR "604" >}}, {{< MR "634" >}}, {{< MR "677" >}}, {{< MR "695" >}}, {{< MR "741" >}}, {{< MR "747" >}}, {{< MR "830" >}}, {{< MR "868" >}}, {{< MR "889" >}}, {{< MR "898" >}}

* Code cleanup and improving coding standards. {{< MR "509" >}}, {{< MR "539" >}}, {{< MR "540" >}}, {{< MR "553" >}}, {{< MR "560" >}}, {{< MR "572" >}}, {{< MR "756" >}}, {{< MR "772" >}}, {{< MR "776" >}}, {{< MR "750" >}}, {{< MR "820" >}}, {{< MR "821" >}}, {{< MR "835" >}}, {{< MR "838" >}}, {{< MR "859" >}}, {{< MR "879" >}}, {{< MR "886" >}}, {{< MR "902" >}}, {{< MR "912" >}}, {{< MR "916" >}}, {{< MR "918" >}}

* Testsuite changes: {{< MR "531" >}}, {{< MR "535" >}}, {{< MR "550" >}}, {{< MR "565" >}}, {{< MR "570" >}}, {{< MR "589" >}}, {{< MR "592" >}}, {{< MR "666" >}}, {{< MR "670" >}}, {{< MR "696" >}}, {{< MR "711" >}}, {{< MR "719" >}}, {{< MR "724" >}}, {{< MR "727" >}}, {{< MR "737" >}}, {{< MR "774" >}}, {{< MR "846" >}}, {{< MR "866" >}}, {{< MR "947" >}}

* Update of the Build system. {{< MR "502" >}}, {{< MR "705" >}}, {{< MR "728" >}}, {{< MR "790" >}}, {{< MR "793" >}}, {{< MR "795" >}}, {{< MR "841" >}}, {{< MR "878" >}}, {{< MR "883" >}}

####  Octopus 9 

#### 9.2 - 2020-02-07  

* Fixed: Set lsize also for the default cubic lattice when {{<variable "LatticeVectors">}} block is not defined. {{< MR "466" >}}
* Fixed MPI error with k-point parallelization. {{< MR "615" >}}
* Bufix relating to restart and {{<variable "TDFreezeU">}}. {{< MR "625" >}}
* Fixed unwanted space for pdos name. {{< MR "626" >}}
* Fixed the name of bader output for spin unpolarized systems. {{< MR "627" >}}
* Fixed: The variable {{<variable "OutputDuringSCF">}} was not used for {{<code "unocc">}} calculations, unlike what the documentation was saying. {{< MR "628" >}}
* Fixed: The Ewald long-range for 2D was leading to NaN for energy or forces for thick slabs. {{< MR "632" >}}
* Fixed: Problem in parsing some non-standard(?) UPF2 and QSO files. {{< MR "635" >}}
* Workaround for a problem with ifort 2018.1 and OpenMP. {{< MR "637" >}}
* Fixed Magnus propagator. {{< MR "655" >}}
* Fixed: some norms were badly handled in the implicit RK propagators. {{< MR "665" >}}
* Bugfix for calculations with real orbitals. {{< MR "694" >}}
* Bugfix to the Ewald2D for large slabs. {{< MR "714" >}}
* Remove experimental states to SG15 pseudo set. {{< MR "715" >}}
* Bugfix: in case of periodic systems, the user-defined and soft-Coulomb potential where not taking into account periodic copies. {{< MR "754" >}}
* Bugfix: the dielectric function utility was not working for dimensions lower than 3. {{< MR "755" >}}
* Fix a wrong IO format. {{< MR "760" >}}
* Fix the kernel for 1D periodic FFTs. {{< MR "761" >}}
* Some test suite updates {{< MR "610" >}}

####  9.1 - 2019-06-28  
* Fixed condition for the {{< file "oct-dielectric-function" >}} utility for the case where the variable {{<variable "GaugeFieldDelay">}} was not present, but {{<variable "TransientAbsorptionReference">}} was. {{< MR "530" >}}
* Fixed a few tests from the testsuite. {{< MR "532" >}} {{< MR "534" >}} {{< MR "580" >}}
* Assume all atoms overlap for periodic systems to avoid some possible problems when applying the non-local projectors. This is a temporary solution and the problem should be fixed in the next major release. {{< MR "551" >}} 
* Fixed a bug with frozen U of frozen occupations in TDDFT+U that was causing a restart problem. {{< MR "567" >}}
* Fixed xcrysden output for non-orthogonal cells. {{< MR "574" >}}
* Fixed bug in the phase application to the wavefunction when using the Taylor exponential method with ETRS and self-consistent ETRS propagators. {{< MR "584" >}}
* Fixed incorrect projected DOS with spin-polarized orbitals. {{< MR "590" >}}
* Fixed bug when using {{<variable "TDFreezeOrbitals">}} and spinors. {{< MR "598" >}}

####  9.0 - 2019-05-14  

* Added a random number generator to the parser. {{< MR "151" >}}
* Added support of VDW TS correction for periodic systems. {{< MR "160" >}}
* Added option to output the total energy density to a file. {{< MR "217" >}}
* Added an option to benchmark the DFT+U part of the code. {{< MR "234" >}}
* The DFT+U can now be used with domain parallelization. {{< MR "238" >}} {{< MR "349" >}}
* Changed the behaviour of the states randomization to generate real states at Gamma. {{< MR "258" >}}
* Support for PSP8 pseudopotential format. {{< MR "265" >}} {{< MR "271" >}}
* Inclusion of local field effects in the calculation of the absorption spectrum of solvated molecules within the PCM framework. {{< MR "266" >}} {{< MR "501" >}} {{< MR "503" >}}
* Several fixes and updates to the implementation of magneto-optics for solids within the Sternheimer approach. {{< MR "267" >}}
* Added the possibility to test the application of the Hamiltonian. {{< MR "269" >}}
* The {{< file "oct-dielectric-function" >}} utility now allows for the calculation of transient absorption. {{< MR "270" >}} {{< MR "483" >}}
* Added G=G'=0 term in the long-range contribution (LRC) to exchange-correlation kernel in Sternheimer approach. {{< MR "274" >}}
* Bugfix for the calculation of forces, if {{<code-inline>}}{{<variable "KPointsUseSymmetries">}}=yes{{</code-inline>}} was used with zero-weight k-points. {{< MR "276" >}}
* Converted the {{< file "oct-test" >}} utility into a calculation mode of {{< octopus >}}. {{< MR "281" >}}
* Adding a contribution to the forces to take into account the SCF convergence, see Phys. Rev. B 47, 4771 (1993). {{< MR "287" >}}
* Adding the force term from the nonlinear core correction that was previously missing. {{< MR "292" >}}
* Set a new default damp factor for exponential and gaussian damping methods in the calculation of spectra. {{< MR "299" >}}
* Add the possibility to construct a DFT+U basis from states taken from a different calculation. {{< MR "302" >}} {{< MR "362" >}}
* Implementation of more Gram-Schmidt orthogonalization schemes. {{< MR "308" >}}
* Add the possibility to select the Gram-Schmidt scheme used for the Arnoldi process (used for Lanczos exponential method). {{< MR "310" >}}
* Removed {{< file "octopus-mpi" >}} dummy executable. {{< MR "327" >}}
* Removed the complex scaling method. {{< MR "329" >}} {{< MR "344" >}}
* Fixed bug in multigrid preconditioner. {{< MR "331" >}}
* Add input option {{<variable "DebugTrapSignals">}} to enable or disable trapping signals inside the code. {{< MR "333" >}} {{< MR "347" >}}
* Added all the input files from the {{<versioned-link "tutorial/basics/" "Octopus basics" >}} tutorials to the testsuite. {{< MR "337" >}}
* k-point parallelization is now used by default. {{< MR "338" >}} {{< MR "358" >}}
* Improved the default CG eigensolver to follow Payne et al. (1992), Rev. Mod. Phys. 64, 4. Added options for orthogonalization against previous/all states, for using additional terms in the energy minimization, and for using different conjugate directions. Added configuration options to multigrid preconditioner. {{< MR "342" >}} {{< MR "478" >}}
* Removed initial attempt to implement subsystem DFT as this was not mature enough. {{< MR "343" >}}
* Allow for changing the amount of screening taken into account in the ACBN0 functional. {{< MR "348" >}}
* Output of the Kanamori interaction parameters. {{< MR "351" >}}
* Add the output of the electron-ion potential as an output of {{<code-inline>}}{{<variable="Output">}}=potential_gradient{{</code-inline>}}. {{< MR "360" >}}
* Implemented the around mean-field double counting term for DFT+U. {{< MR "365" >}} {{< MR "412" >}}
* Improved the mixing for DFT+U GS calculations. {{< MR "367" >}}
* Introduced new variable {{<variable="PropagationSpectrumSymmetrizeSigma">}} to symmetrize the photo-absorption cross-section tensor when calling the {{< file "oct-propagation_spectrum" >}} utility. {{< MR "376" >}}
* Implementation of the rotationally invariant form of the ACBN0 functional. This is now the default option, except for spinors. {{< MR "378" >}} {{< MR "463" >}}
* Modernized build system by replacing the recursive Automake files in {{< file "src" >}} by a single Automake file. {{< MR "396" >}} {{< MR "406" >}} {{< MR "407" >}} {{< MR "414" >}} {{< MR "438" >}} {{< MR "467" >}}
* Added the option to output the testsuite results to a YAML file. {{< MR "405" >}} {{< MR "413" >}} {{< MR "452" >}}
* Instrument {{< octopus >}} to use likwid for performance measurement. {{< MR "417" >}}
* Several improvements to the Cuda support. {{< MR "421" >}} {{< MR "428" >}}
* Renamed XCParallel input variable to ParallelXC and its default value is now "yes". {{< MR "422" >}} {{< MR "423" >}}
* Removed the openscad output format. {{< MR "425" >}}
* Added workaround for bug in GCC C preprocessor on PowerPC. {{< MR "443" >}}
* Use all GPUs on a node with the CUDA backend when running in parallel. {{< MR "446" >}} {{< MR "448" >}}
* Fixed the lattice vectors in the xcrysden outputs. {{< MR "455" >}}
* The option {{<variable="TDFreeOrbitals">}} now supports states parallelization. {{< MR "456" >}}
* Improved stability of the symmetrization. {{< MR "474" >}}
* Improved support for libvdwxc, including new {{< code "--with-libvdwxc-prefix" >}} configure flag. {{< MR "488" >}} {{< MR "499" >}}
* Fixed incorrect installation of header files from libyaml and spglib external libraries. {{< MR "500" >}}
* Fixed bug when checking if {{<variable "GaugeVectorField">}} breaks the symmetries or not. {{< MR "517" >}}
* Add warning about deprecation of Libxc 2 and forbid GGA and MGGA calculations with spinors. {{< MR "507" >}}
* Many optimizations:
  * DFT+U. {{< MR "235" >}} {{< MR "275" >}} {{< MR "374" >}}
  * Application of the non-local part of the pseudopotentials. {{< MR "255" >}} {{< MR "321" >}} {{< MR "322" >}} {{< MR "363" >}} {{< MR "373" >}} {{< MR "399" >}}
  * Application of the phase in the exponential methods. {{< MR "252" >}} {{< MR "323" >}} {{< MR "330" >}} {{< MR "389" >}}
  * Propagators. {{< MR "370" >}}
  * Calculation of the Ewald summation for 2D systems. {{< MR "381" >}}
  * States are now stored in packed mode as default ({{<code-inline>}}{{<variable "StatesPack">}}=yes{{</code-inline>}}) to improve performance, except when running on a GPU. {{< MR "296" >}} {{< MR "457" >}} {{< MR "518" >}}
  * Speed up GPU version by reducing number of allocations. {{< MR "465" >}}
  * Reduced memory used when {{<code-inline>}}{{<variable "StatesPack">}}=yes{{</code-inline>}}. {{< MR "475" >}} {{< MR "476" >}}
  * Varia. {{< MR "246" >}} {{< MR "324" >}} {{< MR "334" >}} {{< MR "335" >}} {{< MR "340" >}} {{< MR "356" >}} {{< MR "464" >}} {{< MR "491" >}}
* Other bug fixes and improvements. {{< MR "196" >}} {{< MR "215" >}} {{< MR "231" >}} {{< MR "251" >}} {{< MR "254" >}} {{< MR "261" >}} {{< MR "282" >}} {{< MR "288" >}} {{< MR "289" >}} {{< MR "294" >}} {{< MR "301" >}} {{< MR "305" >}} {{< MR "309" >}} {{< MR "313" >}} {{< MR "314" >}} {{< MR "317" >}} {{< MR "325" >}} {{< MR "326" >}} {{< MR "332" >}} {{< MR "336" >}} {{< MR "345" >}} {{< MR "350" >}} {{< MR "352" >}} {{< MR "377" >}} {{< MR "391" >}} {{< MR "397" >}} {{< MR "400" >}} {{< MR "401" >}} {{< MR "404" >}} {{< MR "411" >}} {{< MR "415" >}} {{< MR "419" >}} {{< MR "427" >}} {{< MR "429" >}} {{< MR "430" >}} {{< MR "433" >}} {{< MR "435" >}} {{< MR "439" >}} {{< MR "441" >}} {{< MR "444" >}} {{< MR "440" >}} {{< MR "451" >}} {{< MR "454" >}} {{< MR "458" >}} {{< MR "459" >}} {{< MR "461" >}} {{< MR "468" >}} {{< MR "469" >}} {{< MR "471" >}} {{< MR "473" >}} {{< MR "470" >}} {{< MR "477" >}} {{< MR "480" >}} {{< MR "481" >}} {{< MR "482" >}} {{< MR "485" >}} {{< MR "486" >}} {{< MR "489" >}} {{< MR "490" >}} {{< MR "495" >}} {{< MR "496" >}} {{< MR "504" >}} {{< MR "520" >}}

### Octopus 8

####  8.4 - 2019-02-08  

* Fixed incorrect Thallium atomic mass for HGH pseudopotential set with semicore states. {{< MR "384" >}}
* Fixed error when using Cu pseudopotentials, as some of the element data was taken from Curium (Cm). {{< MR "384" >}}
* Fixed incorrect parsing of valence charge in some XML based pseudopotential files. {{< MR "386" >}}
* Fixed bug when using {{<variable "TDFreezeOrbitals">}} with {{<code-inline>}}{{<variable "SpinComponents">}}=spin_polarised{{</code-inline>}} or 
{{<code-inline>}}{{<variable "SpinComponents">}}=spinors{{</code-inline>}}. {{< MR "395" >}}
* Forbid the use of {{<variable "TDFreezeOrbitals">}} with MGGAs. {{< MR "394" >}}
* The calculation of the DOS that was displayed on the screen was not correct when using k-point symmetries or zero-weight k-points. {{< MR "408" >}}
* Fixed problem with spin-polarized version of ADSIC functional which could sometimes result in incorrect results. {{< MR "410" >}}
* Fixed compilation with GCC without using the {{<code "-ffree-line-length-none">}} flag. {{< MR "436" >}}

####  8.3 - 2018-11-13  

* Fixed the value of the {{<variable "localMagneticMomentsSphereRadius">}} variable for the case of one atom in a periodic system. {{< MR "318" >}}
* Fixed the xcrysden output for non-orthogonal cells. {{< MR "354" >}}
* Fixed several bugs in the eigensolvers. {{< MR "355" >}}
* The divergence was not correctly calculated for non-orthogonal cells, making GGA and MGGA calculations incorrect. {{< MR "359" >}}
* {{<code-inline>}}{{<variable "MoveIons">}} = yes{{</code-inline>}} is now forbidden when either NLCC or DFT+U are used, as there is a missing term. {{< MR "361" >}}
* Fixed several missing OpenMP private statements. {{< MR "368" >}}
* Fixed bug in the calculation of the Ewald summation for 2D periodic systems. {{< MR "380" >}}
* Minor fixes. {{< MR "366" >}}

####  8.2 - 2018-08-08  

* Fix a problem of normalization of the NLCC for spinors. {{< MR "293" >}}
* MGGA should not be used with pseudopotentials that have non-linear core-corrections. {{< MR "295" >}}
* Fixed bug when {{<code-inline>}}{{<variable "TDMultipoleLmax">}} = 0{{</code-inline>}}. {{< MR "300" >}}
* Fix bug in Jacobi preconditioner. {{< MR "303" >}}
* KPointsUseTimeReversal input variable is not marked as experimental anymore. {{< MR "306" >}}
* Fixed bug affecting {{<code-inline>}}{{<variable "TDDeltaStrengthMode">}} = kick_spin_and_density{{</code-inline>}}. {{< MR "307" >}}
* Fixed bug in the geometry optimization restart. {{< MR "311" >}}
* Fixed bug when reading geometry optimization constrains from xyz files in Angstroms. {{< MR "312" >}}
* Fixed incorrect display of the parallelepiped simulation box lengths. {{< MR "316" >}}

####  8.1 - 2018-07-06  

* Fix memory leak in Lanczos propagator. {{< MR "241" >}}
* Update experimental status for several features. {{< MR "243" >}}
* Fixed incorrect number of excited electrons in TD output with spin-polarization. {{< MR "247" >}}
* Fix problem with cylinder box for periodic systems. {{< MR "253" >}}
* Fix incorrect path to the {{<file "td.general" >}} folder when using the {{<file "oct-harmonic-spectrum">}} utility. {{< MR "278" >}} {{< MR "284" >}}
* Fix LDA and PBEsol stringent pseudos sets that were using the standard ones instead. {{< MR "279" >}} 
* Several minor fixes and improvements. {{< MR "240" >}} {{< MR "242" >}} {{< MR "260" >}} {{< MR "266" >}} {{< MR "283" >}}

####  8.0 - 2018-06-15  

* Introduced the possibility of adding constrains for a geometry optimization. {{< MR "80" >}}
* Update spglib to version 1.9.9. {{< MR "84" >}} {{< MR "89" >}} {{< MR "207" >}}
* Option to output quantities integrated over a plane. {{< MR "85" >}}
* Lattice vectors can now be specified using angles. {{< MR "86" >}}
* Added two new pseudopotential sets, pseudodojo_lda and pseudodojo_pbe, taken from [pseudo-dojo.org](http://www.pseudo-dojo.org/) . {{< MR "87" >}} {{< MR "170" >}}
* The atomic coordinates are now printed at the beginning of a calculation to {{<file "exec/initial_coordinates" >}}. {{< MR "88" >}}
* Support for Libxc 4. {{< MR "90" >}} {{< MR "195" >}}
* Use of symmetries and reduction of Brillouin zone for non-magnetic periodic systems are now working. {{< MR "96" >}} {{< MR "101" >}}
* Introduced DFT-D3 van der Waals corrections of S. Grimme et al (J. Chem. Phys. 132, 154104 (2010)). {{< MR "99" >}} {{< MR "120" >}} {{< MR "121" >}}
* Information about failed tests is now printed at the end of the test suite run. {{< MR "100" >}}
* The output of the total current is now done for the spin-resolved current. {{< MR "107" >}}
* Calculation of HHG from the current with the oct-harmonic_spectrum utiltiy. {{< MR "117" >}}
* Split the {{<variable "StatesPack" >}} input option into {{<variable "StatesPack">}} and {{<variable "HamiltonianApplyPacked">}}. {{< MR "135" >}}
* New {{<variable "ExtraStatesToConverge">}} input option. {{< MR "143" >}}
* Updated ELPA supported version to 20170403. {{< MR "149" >}}
* The FIRE algorithm is now used by default for Geometry Optimization runs. {{< MR "152" >}}
* New {{<variable "CasidaPrintExcitations">}} and {{<variable "CasidaWeightThreshold">}} input options to control which excitations are written during a Casida calculation. {{< MR "155" >}}
* New {{<variable "OutputGradientPotential">}} input option. {{< MR "161" >}}
* Output of Forces during real-time propagation. {{< MR "165" >}}
* Support for PSML pseudopotential format. {{< MR "166" >}}
* Non-equilibrium Time-dependent Polarizable Continuum Model {{< MR "176" >}} {{< MR "202" >}} 
* Output of electronic and current densities resolved in momentum space. {{< MR "180" >}}
* New set option in the Species block {{< MR "185" >}}
* RMDFT orbital optimization is now done using steepest descent by default. {{< MR "145" >}}
* Default value for XCFunctional is taken, when available, from the pseudopotential. {{< MR "171" >}}
* The {{<file "band-gp.dat" >}} and {{<file "bands-e_fermi.dat" >}} output files have been removed. {{< MR "209" >}}
* New commutator-free Magnus propagator. See section 3.2 of https://arxiv.org/abs/1803.02113 for more details. {{< MR "220" >}}
* DFT+U method. The U can be user defined (empirical), or determined self-consistently using the ACBN0 functional (PRX 5, 011006 (2015)). {{< MR "221" >}} {{< MR "227" >}} {{< MR "228" >}}
* Support for AVX512 vector intrinsics. {{< MR "225" >}}
* Many bugfixes and other improvements. {{< MR "81" >}} {{< MR "91" >}} {{< MR "92" >}} {{< MR "95" >}} {{< MR "98" >}} {{< MR "104" >}} {{< MR "105" >}} {{< MR "108" >}} {{< MR "110" >}} {{< MR "112" >}} {{< MR "115" >}} {{< MR "119" >}} {{< MR "123" >}} {{< MR "125" >}} {{< MR "127" >}} {{< MR "130" >}} {{< MR "134" >}} {{< MR "137" >}} {{< MR "139" >}} {{< MR "141" >}} {{< MR "142" >}} {{< MR "144" >}} {{< MR "146" >}} {{< MR "147" >}} {{< MR "150" >}} {{< MR "153" >}} {{< MR "154" >}} {{< MR "156" >}} {{< MR "157" >}} {{< MR "158" >}} {{< MR "159" >}} {{< MR "163" >}} {{< MR "164" >}} {{< MR "168" >}} {{< MR "169" >}} {{< MR "172" >}} {{< MR "173" >}} {{< MR "175" >}} {{< MR "177" >}} {{< MR "182" >}} {{< MR "186" >}} {{< MR "187" >}} {{< MR "188" >}} {{< MR "189" >}} {{< MR "190" >}} {{< MR "191" >}} {{< MR "193" >}} {{< MR "194" >}} {{< MR "197" >}} {{< MR "198" >}} {{< MR "199" >}} {{< MR "200" >}} {{< MR "201" >}} {{< MR "203" >}} {{< MR "204" >}} {{< MR "205" >}} {{< MR "208" >}} {{< MR "210" >}} {{< MR "212" >}} {{< MR "214" >}} {{< MR "216" >}} {{< MR "218" >}} {{< MR "219" >}} {{< MR "229" >}}

### Octopus 7

####  7.3 - 2018-03-24  

* Fixed bug in <tt>Casida</tt> mode: for matrices of even size, half of the rows to be computed had one entry less than the other half, and some elements were computed twice, while others remained zero. {{< MR "178" >}}

####  7.2 - 2018-01-15  

* Fixed bug when using the <tt>aetrs</tt> propagator along with the <tt>lanczos</tt> method for the application of the exponential. {{< MR "132" >}}

####  7.1 - 2017-06-09  

* Fixed file missing from tarball. {{< MR "83" >}}

####  7.0 - 2017-06-06  

* Support for CUDA.
* Improved assertions and error checking in the input file parser and added possibility to include another file into the input file.
* More flexible jellium volumes: The volumes can be constructed by summing and subtracting spheres and slabs.
* Scissors operator for TD calculations.
* Calculation of stress tensor.
* Calculation of band structures by using the unocc run mode and by defining k-point paths.
* New selection of MD integrators for the Fire algorithm.
* Many bug fixes.

### Octopus 6

####  6.0 - 2016-09-05  

* New numbering scheme. Now all releases have a major and minor number. Changes in the minor number indicate bug fix releases.
* The <tt>octopus_mpi</tt> executable is gone. Now the <tt>octopus</tt> binary should be used in all cases.
* New simpler and more flexible interface for Species block. This makes it easier to use external pseudopotentials.
* Pseudopotential sets, included sets are HGH, SG15, and HSCV.
* Support for UPF2 and ONCV pseudopotentials.
* van der Waals corrections.
* Support for [[libxc 3.0.0]].
* Solvation effects by using the Integral Equation Formalism Polarizable Continuum Model (IEF-PCM)

### Octopus 5

####  5.0.1 - 2016-01-11  

* Several bug fixes to 5.0.0, including supporting GSL 2.0 and use of gcc version 5.0 or later for preprocessing Fortran.

####  5.0.0 - 2015-10-07  

* Casida: excited-state forces and complex wavefunctions
* More helpful treatment of preprocessor in configure script
* Improvements and bugfixes to LCAO and unocc mode
* Non-self-consistent calculations starting only from density in unocc (''e.g.'' for bandstructure)
* Utilities can be run in serial without MPI, even after compiling the code with MPI.
* Hybrid meta-GGAs are enabled.
* Bugfixes for partially periodic systems.
* Linear-response vibrational modes: restart from saved modes and numerical improvements
* FIRE algorithm for geometry optimization
* OpenSCAD output for geometries and fields
* VTK legacy output for scalar fields
* More flexible and safer restart
* Parallel mesh partitioning using Parmetis
* New libISF Poisson solver

### Octopus 4

####  4.1.2 - 2013-11-18  

* Several bug fixes to 4.1.1, including a critical bug that affected the calculation of GGA and MGGA exchange and correlation potentials.

####  4.1.1 - 2013-09-03  

* Several bug fixes to 4.1.0.

####  4.1.0 - 2013-06-12  

* Several bug fixes.
* Compressed sensing for the calculation of spectra.
* Improved GPU support.
* Support for Libxc 2.0.x
* Tamm-Dancoff approximation, CV(2) theory, and triplet excitations, in Casida run mode
* PFFT 1.0.5 implementation as Poisson solver
* FMM implementation through Scafacos library
* Faster initialization
* Added Doxygen comments to the source code
* Support for BerkeleyGW output.

####  4.0.1 - 2012-02-03  

* Several bug fixes to 4.0.0

####  4.0.0 - 2011-06-19  

* Many bugfixes.
* Several improvements for calculations on periodic systems.
* Libxc is now a stand alone library.
* Improved vectorization.
* Experimental Scalapack parallelization.
* Experimental OpenCL support (for GPUs).

### Octopus 3

####  3.2.0 - 2009-11-24  

* Improved parallelization.
* Support for cross compiling.
* Initial support for Meta-GGAs.
* Fixed several bugs with UPF pseudopotentials (they are still considered under development, though).
* 4D runs for model systems.
* Inversion of Kohn-Sham equation.
* Hybrids with non-collinear spin.
* Several new functionals in libxc.
* Optimizations for Blue Gene/P systems.
* Compilation fixes for several supercomputer platforms.

####  3.1.1 - 2009-11-04  

* Fixed a dead-lock in time propagation.
* Fixed unit conversion in the output.
* Fixed a bug with parallel multigrid.
* Disabled UPF pseudopotentials.
* Hybrid OpenMP/MPI parallelization.

####  3.1.0 - 2009-04-14  

* Pseudo-potential filtering.
* Car-Parrinello molecular dynamics.
* Parallelization over spin and k-points.
* Improved parallelization over domains.
* Improved calculation of unoccupied states. 
* Removed support for FFTW2.
* Small performance optimizations.

####  3.0.1 - 2008-05-08  

* Several bug fixes to 3.0.0

####  3.0.0 - 2008-02-19  

* '''Physics'''
  * Hartree-Fock approximation
  * Hybrid XC functionals, more GGA functionals.
  * Sternheimer linear-response calculation of:
    * First-order dynamic hyperpolarizabilities.
    * Magnetic susceptibilities (experimental).
    * Van der Waals coefficients.
    * Vibrational frequencies and infrared spectra.
  * Optimal control theory.
  * Fast Ehrenfest molecular dynamics (http://arxiv.org/abs/0710.3321).
  * Circular dichroism.
* '''Algorithms'''
  * Improved geometry optimization.
  * More precise calculation of forces.
  * Double-grid support.
  * Preconditioning for the ground state and Sternheimer linear response.
  * Interpolating scaling functions Poisson solver (J. Chem. Phys. 125, 074105 (2006)).
  * More pseudopotential formats supported.
* '''Parallelization and optimization''':
  * Non-blocking MPI communication.
  * OpenMP parallelization that can be combined with MPI.
  * Optimized inner loops, including hand-coded vector routines for x86 and x86_64 and assembler code for Itanium.
  * Optimized scheme to store non-local operators that results in higher performance, better scalability and less memory consumption.
  * Single-precision version (experimental).
* '''Other''':
  * New platform-independent binary format for restart files (old restart files are incompatible).
  * {{< file "oct-help" >}} command-line utility.

### Octopus 2

####  2.1.0 - 2007-06-05  

* The complex executable is gone, all the work is done by the normal (a.k.a real) executable. The type of the wavefunctions is selected automatically according to the input file.
* Calculation of dynamical polarizabities using linear-response theory.
* Basic support for full-potential all-electron species.
* The texinfo documentation has been obsoleted and replaced by an [[Manual|online wiki-based documentation]].
* Debian packages are generated using the gfortran compiler.

####  2.0.1 - 2006/03/23  

* Fixed bugs for the following cases:
  * Spin-unrestricted calculations for systems with non-local pseudopotentials were giving wrong numbers. For some atoms, the error was small, but there were cases for which the errors were sizeable.
  * Wrong units were used in a part of the Vosko, Wilk & Nusair correlation functional.
  * Parallel calculations with orbital-dependent xc functionals crashed under some circumstances.
  * Local magnetic moments were not computed properly when running parallel in domains.

####  2.0.0 - 2006/02/17  

* Curvilinear coordinates: This is one of the main novelties. A general framework is implemented, and several different curvilinear systems are implemented. At this moment only the coordinate transformation of François Gygi is working well. The efficiency gained from using curvilinear coordinates can be as large as a factor of 2 or 3 in computational time. However, note that this method is not well suited if you want to move the ions.
* Parallelization in domains: You will be able to run octopus in parallel in domains. This means that not only the real time is reduced, but also the memory is shared among the different nodes. Also some parts of the code will have a mixed parallelization. For example, the time-dependent propagation will be parallel both in domains and in states. This was basically the work of [[Florian|Florian Lorenzen]] and [[Heiko|Heiko Appel]].
* Static response properties: Now Octopus can calculate the static polarizability and the first static hyperpolarizability. These are obtained by solving a Sternheimer equation. We have also planned the computation of vibrational properties. This was done mainly by [[Xavier|Xavier Andrade]] and [[Hyllios]]. Instructions {{< manual "Linear Response" "here" >}}
* A larger selection of exchange-correlation functionals. This came with the development of the NANOQUANTA xc library. We have by now <i>all</i> LDAs and some GGAs. This library is also used by the newest version of [http://www.abinit.org/ ABINIT]. We expect that the number of xc functionals available will increase rapidly.
* A multigrid solver for the Poisson equation. Implemented by [[user:Xavier|Xavier Andrade]]. (Note: not working with curvlinear coordinates)
* Periodic Systems. Finally, the ground state seems to work at least for cubic systems. There is still some work remaining in order to run a time-dependent simulation. Implemented by [[user:Carlo|Carlo Rozzi]] and [[Heiko|Heiko Appel]].
* Spinors and Spin Orbit. Yes, spin-orbit works but only with HGH pseudopotentials. The calculations are quite heavy, though ;). Brought to you by [[Alberto|Alberto Castro]] and [[user:Micael|Micael Oliveira]].

* Multi Subsystem Mode. Like [http://www.abinit.org/ ABINIT], now you can perform multiple runs with only one input file. Implemented by [[Heiko|Heiko Appel]].

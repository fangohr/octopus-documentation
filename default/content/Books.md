---
title: "Books"
weigth: 200
---


* {{< book title="Fundamentals of Time-Dependent Density Functional Theory" author="M. A. L. Marques and N. Maitra and F. Nogueira and E. K. U. Gross and A. Rubio (editors)" publisher="Springer Berlin Heidelberg New York" isbn="3-642-23517-4" series="Lecture Notes in Physics" year="2012" >}}

* {{< book title="Time-Dependent Density Functional Theory" author="M. A. L. Marques and C. A. Ullrich and F. Nogueira and A. Rubio and K. Burke and E. K. U. Gross (editors)" publisher="Springer Berlin Heidelberg New York" isbn="3-540-35422-0" series="Lecture Notes in Physics" year="2006" issn="0075-8450" >}}

* {{< book title="Photons and Atoms: Introduction to Quantum Electrodynamics" author="C. Cohen-Tannoudji, J. Dupont-Roc, and G. Grynberg" publisher="Wiley VCH" year="2004" isbn="0471184330" >}}

* {{< book title="Atom-Photon Interactions: Basic Processes and Applications" author="C. Cohen-Tannoudji, J. Dupont-Roc, and G. Grynberg" publisher="Wiley VCH " year="2004" isbn="0471293369" >}}

* {{< book title="Molecular Quantum Electrodynamics" author="D. P. Craig and T. Thirunamachandran" publisher="Dover" year="1998" isbn="0486402142" >}}

* {{< book title="Relativistic Quantum Mechanics" author="Paul Strange" publisher="Cambridge University Press" year="1998" isbn="0521565839" >}}

* {{< book title="The Electron-Phonon Interaction in Metals" author="G. Grimvall" publisher="North-Holland" year="1980" isbn="044486105 X" >}}

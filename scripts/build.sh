#!/usr/bin/env bash


branch=`scripts/get_build.py`
name=`echo $branch | cut -d"." -f 1`

sed "{s|OCTOPUSVERSION|$name|g;s|OCTOPUSBRANCH|$branch|g;s|SITE_PREFIX|$PREFIX|g}" config.toml_TEMPLATE > config.toml
cat config_toml.end >> config.toml

echo "building branch "$branch

rm -rf public/$name
rm -rf public/$PREFIX/$name

hugo --destination "public/$PREFIX/$name" > $name.log 2>&1

rm -rf static/images static/testsuite static/doc static/octopus static/includes
rm -rf static/*.txt
rm -rf static/graph_data
rm content/*


rm config.toml

if [! -f $name.log ]; then 
    exit -1
fi
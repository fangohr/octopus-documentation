#!/usr/bin/env python3


# Copyright (C) 2020 Martin Lueders 
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.
#


import sys
import os.path
import glob
import re
import json
import getopt

srcdir="octopus/src/"

try:
    options, args = getopt.getopt(sys.argv[1:], "d:s:", ['srcdir='])
except  getopt.GetoptError:
    print('Error !')
    usage()
    sys.exit(-1)

for (opt, arg) in options:
    if opt in ['-d', '-s', '--srcdir']:
        srcdir = arg


srcfiles = glob.glob(srcdir+"*/*.F90")

classes = dict()
inverse = dict()
parents = set()
children= set()
abstract = set()


for f in srcfiles:
    with open(f,'r') as source:

        lines = source.readlines()
        for line in lines:
            if 'type' in line and 'abstract' in line:
                class_name = line.split(':')[-1].strip()
                abstract.add(class_name)

            if 'extends(' in line:
                class_name = line.split(':')[-1].strip()
                parent_name = re.findall('extends\((\w+)\)', line)[0]

#                print(f+": "+class_name+' '+parent_name)

                parents.add(parent_name)
                children.add(class_name)

                if 'abstract' in line:
                    abstract.add(class_name)

                if parent_name not in classes:
                    classes[parent_name] = [class_name]
                else:
                    classes[parent_name].append(class_name)

                inverse[class_name] = parent_name

top = (parents.difference(children))

counter=ord('A')

def flatten(x, my_set, prefix=''):
    my_set1 = my_set.copy()
    my_set1.add(x)

#    print(prefix+'-- calling flatten for: '+x+' with '+','.join(my_set))
    if x in classes:
        for c in classes[x]:
            new_elements = flatten(c,my_set1, prefix+'_')
            my_set1.update(new_elements)
#    else:
#        my_set1.add(x)

#    print(prefix+'== flatten for: '+x+' returning '+','.join(my_set1))

    return my_set1


for t in top:

    #output = sys.stdout 
    output = open('static/graph_data/'+t+'.viz','w')

    index = dict()
    counter = ord('A')
    all = flatten(t, set())

    print('digraph  {', file=output)
    print('rankdir=BT;', file=output)
    print('fontsize="6";', file=output)
    print('node [shape="rect", fillcolor=yellow, style="filled", tooltip=" ", fontsize="10"]', file=output)


    for c in all.union({t}):
        index[c] = chr(counter)
        if c in abstract:
            print(chr(counter)+' [label=\"'+c+'\", style=\"rounded, filled\"]', file=output)
        else:
            print(chr(counter)+' [label=\"'+c+'\"]', file=output)
        counter += 1

    for c in all:
        if c in inverse:
            print(index[c]+' -> '+index[inverse[c]], file=output)

    print('}', file=output)
    output.close()

#!/usr/bin/env bash

echo "Copy files:"

scripts/copy_files.sh

echo "Process variables"

scripts/process_variables.sh > variables.out

echo "Process includes:"

scripts/process_includes.sh


